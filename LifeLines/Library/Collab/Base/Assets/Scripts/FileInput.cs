﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileInput : MonoBehaviour
{

    ValueDump vd;
    public Dictionary<string, string> dict = new Dictionary<string, string>();
    Dictionary<string, string> dic = new Dictionary<string, string>();

    // Start is called before the first frame update
    void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
        ReadTextFile();

        //Debug.Log("Contextdic[sportscar]: " + dic["sportscar"].ToString());
        //Debug.Log("Contextdic[bike]: " + dic["bike"].ToString());
        //Debug.Log("Contextdic[apple]: " + dic["apple"].ToString());
        /*dic = vd.GetContextTexts();
        Debug.Log("Contextdic[kaese]: " + dic["kaese"].ToString());
        */
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReadTextFile()
    {
        string relFilePath = "Assets\\Texts\\";
        //string language = vd.GetLanguage();
        //string fileName = language + ".txt";


        string filePath = relFilePath + "ContextTexts.txt";

        StreamReader inputStream = new StreamReader(filePath);
        string input = "";




        input = inputStream.ReadToEnd();
        string[] str = input.Split('/');
        if (str.Length % 2 == 1)
        {
            for (int i = 0; i < str.Length; i++)
            {
                dict.Add(str[i], str[i + 1]);
                foreach (KeyValuePair<string, string> pair in dict)
                {
                    //_inputfield.text = pair.Value.ToString();
                }
            }
        }
        else
        {
            for (int i = 0; i < str.Length; i += 2)
            {
                dict.Add(str[i], str[i + 1]);
                foreach (KeyValuePair<string, string> pair in dict)
                {
                    //_inputfield.text = pair.Value.ToString();
                }

            }
        }




        foreach (var key in dict.Keys)
        {
            //Debug.Log("dict: " + key);
        }

        vd.SetContextTexts(dict);
        inputStream.Close();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsDebug : MonoBehaviour
{   
    //used for displaying the optionsmenu
    //author ~~Jonathan

    public GameObject player;
    public CameraScript cs;
    public ValueDump vd;
    private void Start()
    {
        //player = GameObject.FindGameObjectWithTag("Player");
        //rc = player.GetComponentInChildren<Raycast>();
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
    }

    //show the optionsmenu and disable the player and camera movement
    public void ShowOptions()
    {
        Debug.Log("Ich werde angezeigt");
        this.gameObject.SetActive(true);
        DisableScripts();
    }

    //disable the optionsmenu and enable the scripts
    public void CloseOptions()
    {
        Debug.Log("Ich schließe mich");
        this.gameObject.SetActive(false);
        EnableScripts();
    }

    //disable scripts
    public void DisableScripts()
    {
        //player.GetComponent<PlayerScript>().enabled = false;
        //cs.enabled = false;
        vd.SetInteractable(false);
    }

    //enable scripts
    public void EnableScripts()
    {
        //player.GetComponent<PlayerScript>().enabled = true;
        //cs.enabled = true;
        
        if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
        {
            
        }
        else
        {
            Cursor.visible = false;
        }
        vd.SetInteractable(true);
    }



}

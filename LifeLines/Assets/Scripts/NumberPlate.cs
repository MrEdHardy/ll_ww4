﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberPlate : MonoBehaviour
{
    public GameObject interactionMenu,hitObject;
    public InputField inputFieldPlate;
    public TextMesh[] plateText;
    public ValueDump vd;
    public GameObject player, cursor;

    public QuestLogHandler qlh;
    private void Start()
    {
        
        
    }

    public void MakeMeVisible(GameObject hitObject, ValueDump valueDump)
    {
        Cursor.visible = true;
        plateText[0].text = "";
        plateText[1].text = "";
        plateText[2].text = "";
        interactionMenu.SetActive(true);
        this.hitObject = hitObject;

        vd = valueDump;
        vd.SetInteractable(false);
        cursor.SetActive(false);
    }

    public void HideMe()
    {
        Cursor.visible = false;
        interactionMenu.SetActive(false);
        
    }

    public void EnableScripts()
    {
        vd.SetInteractable(true);
        cursor.SetActive(true);
    }

    public void MakeMyPlate()
    {
        string input = inputFieldPlate.text;
        string rest = "-LL 1337";

        if (hitObject.name.Equals("NumberPlate_FamilyCar"))
        {
            plateText[0].text = input + rest;
        }
        else if (hitObject.name.Equals("NumberPlate_Offroader"))
        {
            plateText[1].text = input + rest;
        }
        else if(hitObject.name.Equals("NumberPlate_SportsCar"))
        {
            plateText[2].text = input + rest;
        }

        //plateText.text = input + rest;

        vd.SetLoc(input);
        qlh.CompleteQuest(6);
        Debug.Log("Auswahl: " + input);
    }

    public void ParseMyPlate(string car, string platetext)
    {
        string input = platetext;
        string rest = "-LL 1337";

        if (car.Equals("Familycar"))
        {
            plateText[0].text = input + rest;
        }
        else if (car.Equals("Offroader"))
        {
            plateText[1].text = input + rest;
        }
        else if (car.Equals("Sportscar"))
        {
            plateText[2].text = input + rest;
        }
    }

}

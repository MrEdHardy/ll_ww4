﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using UnityEngine.SceneManagement;
using System;

public class JsonStuff : MonoBehaviour
{
    /*//public string[] str;
    Dictionary<string,string> dict;
    Dictionary<string, string> dict1;
    Dictionary<string, string>[] ada;
    StreamWriter sw;
    
    
    

    private void Start()
    {
        sw = new StreamWriter("eval.json");
        ada = new Dictionary<string, string>[3];
        dict = new Dictionary<string, string>();
        dict1 = new Dictionary<string, string>();

    }
    public void ToJSON()
    {
        dict.Add("fisch", "dorsch");
        dict.Add("käse", "gouda");
        dict1.Add("fisch", "dorsch");
        dict1.Add("käse", "gouda");

        ada[0] = dict;
        ada[1] = dict1;
        stuff();
    }

    public void stuff()
    {
        string tojason =JsonConvert.SerializeObject(ada, Formatting.Indented);
        using (sw)
        {
            sw.Write(tojason);
        }
        Debug.Log("JSON: " + tojason); 
    }*/

    //used to build the dictionaries from data collected in the scene
    //author ~~Jonathan

    private ValueDump vd;

    //get example objects from all gameobjects with the decisionhandler script on them
    DecisionHandler exampleobject_info, exampleobject_komm, exampleobject_food, example_car, example_bike, example_propulsion;
    string actualfilepath;
    DateTime time;
    //where to save the json file to
    //string fileName = "eval.json";
    private void Awake()
    {
        //check if the file already exists
        /*if(File.Exists(fileName) == true)
        {
            Debug.Log("Die Datei existiert bereits");
            File.Delete(fileName);
        }*/
    }
    private void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();

        //check the scene
        if (SceneManager.GetActiveScene().name.Equals("Scene2"))
        {

        }
        else if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
        {
            if (GameObject.FindGameObjectWithTag("Apfel") == true)
            {
                Debug.Log("Apfel wurde vorher ausgewählt");
                exampleobject_food = GameObject.FindGameObjectWithTag("Apfel").GetComponent<DecisionHandler>();
            }
            else
            {
                Debug.Log("Schokolade wurde vorher ausgewählt");
                exampleobject_food = GameObject.FindGameObjectWithTag("Schokolade").GetComponent<DecisionHandler>();
            }

            if (GameObject.FindGameObjectWithTag("PC") == true)
            {
                exampleobject_info = GameObject.FindGameObjectWithTag("PC").GetComponent<DecisionHandler>();
            }
            else
            {
                exampleobject_info = GameObject.FindGameObjectWithTag("Zeitung").GetComponent<DecisionHandler>();
            }

            if (GameObject.FindGameObjectWithTag("Laptop") == true)
            {
                exampleobject_komm = GameObject.FindGameObjectWithTag("Laptop").GetComponent<DecisionHandler>();
            }
            else if (GameObject.FindGameObjectWithTag("Handy"))
            {
                exampleobject_komm = GameObject.FindGameObjectWithTag("Handy").GetComponent<DecisionHandler>();
            }
            else
            {
                exampleobject_komm = GameObject.FindGameObjectWithTag("Briefumschlag").GetComponent<DecisionHandler>();
            }


            example_car = GameObject.FindGameObjectWithTag("Carpic").GetComponent<DecisionHandler>();
            //example_propulsion = GameObject.FindGameObjectWithTag("Electro").GetComponent<DecisionHandler>();
            example_bike = GameObject.FindGameObjectWithTag("Bike").GetComponent<DecisionHandler>();
        }

    }

    public void BuildDic()
    {
        //build the dictionary based on what scene were in
        string scene = "Szene: ";
        string sceneindex = vd.GetSceneIndex().ToString();

        if (SceneManager.GetActiveScene().name.Equals("Scene2"))
        {

            Dictionary<string, string> scene2 = new Dictionary<string, string>();
            scene2.Add(scene, sceneindex);
            scene2.Add("Jobless", vd.GetJobless());
            //scene2.Add("Jobselection", vd.GetJobselection());
            scene2.Add("Degree", vd.GetDegree());
            scene2.Add("Profession", vd.GetProfession());
            scene2.Add("Career", vd.GetCareer());

            vd.AddDics(scene2);

        }
        else if (SceneManager.GetActiveScene().name.Equals("Scene3"))
        {
            Dictionary<string, string> scene3 = new Dictionary<string, string>();
            scene3.Add(scene, sceneindex);
            scene3.Add("Jobless", vd.GetJobless());
            //scene2.Add("Jobselection", vd.GetJobselection());
            scene3.Add("Degree", vd.GetDegree());
            scene3.Add("Profession", vd.GetProfession());
            scene3.Add("Career", vd.GetCareer());

            vd.AddDics(scene3);
        }
        else if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
        {
            string selection = vd.GetPic().ToString();
            string pic = "Wallpicture";

            string food = exampleobject_food.theme;
            string info = exampleobject_info.theme;
            string komm = exampleobject_komm.theme;
            string car = example_car.theme;
            string propulsion = "Propulsion";
            string bike = example_bike.theme;

            Dictionary<string, string> scene1 = new Dictionary<string, string>();
            scene1.Add(scene, sceneindex);
            scene1.Add(pic, selection);
            scene1.Add(food, vd.GetFood());
            scene1.Add(info, vd.GetInfodevice());
            scene1.Add(komm, vd.GetKommdevice());
            scene1.Add(car, vd.GetCar());
            scene1.Add(propulsion, vd.GetPropulsion());
            scene1.Add(bike, vd.GetBike());
            scene1.Add("Location", vd.GetLocation());
            scene1.Add("PartnerGender", vd.GetGenderPartner());
            scene1.Add("Children", vd.GetChildren());

            vd.AddDics(scene1);
        }




    }

    //Experimental
    //the method that converts a list of dictionaries in a json string; gets written into a file
    public void buildit()
    {


        string tojason = JsonConvert.SerializeObject(vd.GetDics(), Formatting.Indented);


        //if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        //{
        string fileLoc = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        string extendedfileloc = fileLoc + "/Temp";

        if (Directory.Exists(extendedfileloc))
        {
            Debug.Log("It works fine");
            Debug.Log("AppData: " + extendedfileloc);

            if (Directory.Exists(extendedfileloc + "/Lifelines"))
            {
                Debug.Log("nicu desu~");

                if (File.Exists(extendedfileloc + "/Temp/Lifelines/antragsdaten" + time.Year.ToString() + "." + time.Month.ToString() + "." + time.Day.ToString() + " - " + time.Hour.ToString() + "." + time.Minute.ToString() + "." + time.Second.ToString() + ".txt"))
                {
                    File.Delete(extendedfileloc + "/Temp/Lifelines/antragsdaten" + time.Year.ToString() + "." + time.Month.ToString() + "." + time.Day.ToString() + " - " + time.Hour.ToString() + "." + time.Minute.ToString() + "." + time.Second.ToString() + ".txt");
                    Debug.Log("File exists... not anymore");
                }

                time = System.DateTime.Now;
                Debug.Log("time: " + time);
                actualfilepath = extendedfileloc + "/Lifelines/antragsdaten" + time.Year.ToString() + "." + time.Month.ToString() + "." + time.Day.ToString() + " - " + time.Hour.ToString() + "." + time.Minute.ToString() + "." + time.Second.ToString() + ".txt";
            }
            else
            {
                Directory.CreateDirectory(extendedfileloc + "/Lifelines");
                Debug.Log("LL erstellt nice!");
                time = System.DateTime.Now;
                Debug.Log("time: " + time);

                actualfilepath = extendedfileloc + "/Lifelines/antragsdaten" + time.Year.ToString() + "." + time.Month.ToString() + "." + time.Day.ToString() + " - " + time.Hour.ToString() + "." + time.Minute.ToString() + "." + time.Second.ToString() + ".txt";

            }
        }
        //}
        using (StreamWriter sw = new StreamWriter(@actualfilepath))
        {
            sw.Write(tojason);
        }
        Debug.Log("JSON: " + tojason);
    }
}

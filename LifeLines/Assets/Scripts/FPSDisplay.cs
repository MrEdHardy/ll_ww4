﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour
{   

    //used to show a fps counter in the scene
    //author ~~Jonathan

    //set up
    float deltaTime = 0.0f;
    public Text fps;

    void Update()
    {   
        // add and set time from frame to frame minus the deltatime multiplied by 0.1 to deltatime
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        showfps();
    }

    public void showfps()
    {
      

        //calculate the fps
        float msec = deltaTime * 1000.0f;
        float fps1 = 1.0f / deltaTime;
        string text1 = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps1);
        
        fps.text = text1;
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionHandler : MonoBehaviour
{   
    //is used to define decisions on interactable gameobjects
    //author ~~Jonathan
    

    //set up
    public string theme; //is set in the inspector for each gameobject individually

    [SerializeField]
    private string dec; //dito
    ValueDump vd;

    private void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
    }

    public void MakeDecision()
    {   
        //check what decision is needed
        if (theme.Equals("Kommdevice"))
        {   
            //set the value in valuedump
            vd.SetKommDevice(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetKommdevice());
        }
        else if (theme.Equals("Infodevice"))
        {
            vd.SetInfodevice(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetInfodevice());
        }
        else if (theme.Equals("Food"))
        {
            vd.SetFood(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetFood());
        }
        else if (theme.Equals("Jobless"))
        {
            vd.SetJobless(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetJobless());
        }
        else if (theme.Equals("Jobselection"))
        {
            vd.SetJobselection(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetJobselection());
        }
        else if (theme.Equals("Car"))
        {
            vd.SetCar(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetCar());
        }
        else if (theme.Equals("Propulsion"))
        {
            vd.SetPropulsion(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetPropulsion());
        }
        else if (theme.Equals("Bike"))
        {
            vd.SetBike(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetBike());
        }
        else if (theme.Equals("Profession"))
        {
            vd.SetProfession(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetProfession());
        }
        else if (theme.Equals("Degree"))
        {
            vd.SetDegree(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetDegree());
        }
        else if (theme.Equals("Career"))
        {
            vd.SetCareer(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetCareer());
        }
        else if (theme.Equals("shopping")) {
            vd.SetShopping(dec);
            Debug.Log("Theme: " + theme + " Choice: " + dec);
            Debug.Log("VD: " + vd.GetShopping());
        }

        
       
    }
}

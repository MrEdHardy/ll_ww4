﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public class FileInput : MonoBehaviour
{
    //AssetDatabase asset;
    ValueDump vd;
    Dictionary<string, string> dictcontext = new Dictionary<string, string>();
    Dictionary<string, string> dictquestlog = new Dictionary<string, string>();
    Dictionary<string, string> dic = new Dictionary<string, string>();

    List<string> questlogkeys = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
        ReadTextFileContext();
        ReadQuestLogFile();

        //Debug.Log("Contextdic[door]: " + dic["door"].ToString());
        //Debug.Log("Contextdic[bike]: " + dic["bike"].ToString());
        //Debug.Log("Contextdic[apple]: " + dic["apple"].ToString());
        /*dic = vd.GetContextTexts();
        Debug.Log("Contextdic[kaese]: " + dic["kaese"].ToString());
        */
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReadTextFileContext()
    {
        string relFilePath = "Texts/ContextTexts";
        //string language = vd.GetLanguage();
        //string fileName = language + ".txt";


        //string filePath = relFilePath + "ContextTexts.txt";

        //AssetDatabase.ImportAsset(filePath);
        TextAsset contexttext = Resources.Load<TextAsset>(relFilePath);

        //StreamReader inputStream = new StreamReader();
        string input = contexttext.text;


        

        //input = inputStream.ReadToEnd();
        string[] str = input.Split('/');
        if (str.Length % 2 == 1)
        {
            for (int i = 0; i < str.Length; i++)
            {
                dictcontext.Add(str[i], str[i + 1]);
                foreach (KeyValuePair<string, string> pair in dictcontext)
                {
                    //_inputfield.text = pair.Value.ToString();
                }
            }
        }
        else
        {
            for (int i = 0; i < str.Length; i += 2)
            {
                dictcontext.Add(str[i], str[i + 1]);
                foreach (KeyValuePair<string, string> pair in dictcontext)
                {
                    //_inputfield.text = pair.Value.ToString();
                }

            }
        }



        vd.SetContextTexts(dictcontext);
        foreach (var key in dictcontext.Keys)
        {
            //Debug.Log("dict: " + key);
        }

        
        //inputStream.Close();
    }

    public void ReadQuestLogFile()
    {
        string relFilePath = "Texts/QuestLogText";
        TextAsset questlogtext = Resources.Load<TextAsset>(relFilePath);

        string input = questlogtext.text;

        string[] str = input.Split('/');

        if (str.Length % 2 == 1)
        {
            for (int i = 0; i < str.Length; i++)
            {
                dictquestlog.Add(str[i], str[i + 1]);
                foreach (KeyValuePair<string, string> pair in dictcontext)
                {
                    //_inputfield.text = pair.Value.ToString();
                }
            }
        }
        else
        {
            for (int i = 0; i < str.Length; i += 2)
            {
                dictquestlog.Add(str[i], str[i + 1]);
                foreach (KeyValuePair<string, string> pair in dictcontext)
                {
                    //_inputfield.text = pair.Value.ToString();
                }

            }
        }
        vd.SetQuestLogDic(dictquestlog);
        foreach(var keys in dictquestlog.Keys)
        {
            questlogkeys.Add(keys);
        }
        vd.SetQuestLogKeys(questlogkeys);
        Debug.Log("Print that shite: " + questlogkeys[0] + " " + questlogkeys[1]);
    }
}

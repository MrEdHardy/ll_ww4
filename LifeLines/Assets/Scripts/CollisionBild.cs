﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionBild : MonoBehaviour
{   
    //deprecated; used in a previous version of the project
    //was used to handle the trigger event if you collided with a picture you wanted to choose to be portrayed in the large frame
    //author ~~Jonathan

    //Variable setup
    public int bild;
    PicHandler parent;
    ValueDump vd;
    private void Start()
    {   
        //get the pic handler script from the parent gameobject and the valuedump
        parent = this.GetComponentInParent<PicHandler>();
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
    }

    private void OnTriggerEnter(Collider other1)
    {

        //save the chosen pic in valuedump and set the pic in pichandler

        Debug.Log("Du hast was gewählt");
        vd.SetPic(bild);
        parent.GetPicture();

       

    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Hello");
    }

}

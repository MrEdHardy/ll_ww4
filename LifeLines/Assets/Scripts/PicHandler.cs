﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicHandler : MonoBehaviour
{   
    //used to display the chosen pic from the pic selection in the large pic frame
    //author ~~Jonathan

    [SerializeField]
    private GameObject[] pics;

    private GameObject[] picts;
    GameObject value;
    public ValueDump vd;

    Raycast player;

    int picture;
    bool doit;

    private void Start()
    {
        /*value = GameObject.FindGameObjectWithTag("ValueDump");
        if(value != null)
        {
            vd = value.GetComponent<ValueDump>();
        }
        
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Raycast>();*/
        ShowPic(vd.GetPic());
        Debug.Log("StartPic: " + vd.GetPic());
    }

    private void Update()
    {   
        //get the pic when the frame is interacted with
        doit = vd.GetInteracted();
        if (doit == true)
        {
            GetPicture();
        }
    }

    //get the picture from the valuedump
    public void GetPicture()
    {
        Debug.Log("GetPic: " + vd.GetPic());
        int pic = vd.GetPic();
        ShowPic(pic);
    }

    //show the pic in the frame
    public void ShowPic(int bild)
    {

        Debug.Log("it works " + (bild));
        Instantiate(pics[bild]);
        vd.SetInteracted(false);
    }

    //outsourced
    public void DelPic()
    {
        picts = GameObject.FindGameObjectsWithTag("Pic");
        Debug.Log("ich versuche zu löschen");
        Debug.Log(picts.ToString());
        if (picts != null)
        {
            Debug.Log("Array ist leer");
            foreach (GameObject pic in picts)
            {
                Destroy(pic);
            }

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSceneShopping : MonoBehaviour {
    public Canvas bioMilkTE, background, normalMilkTE, t_PaymentBackground, ecCanvas, visaCanvas, barCanvas, paypalCanvas, paybackCanvas,
                    backgroundSM, PaymentBackgroundSM, ecCanvasSM, visaCanvasSM, barCanvasSM, paypalCanvasSM,
                    paybackCanvasSM, bioAppleTE, normalAppleTE, cerealBarTE, chocolatBarTE, bioMilkSM, normalMilkSM,
                    bioAppleSM, normalAppleSM, cerealBarSM, chocolatBarSM;

    public Button btShoppingEndTE, btShoppingEndSM;

    ValueDump vd = new ValueDump();
    SceneTrigger sceneTrigger = new SceneTrigger();
    int productCounter = 0;

    public string shoppingDesc = "";
    // author Marcel
    // Script is on EventSystem
    // Start is called before the first frame update
    void Start() {
        shoppingDesc = vd.GetShopping();
        //shoppingDesc = "smallmarket";
        print("shopping method: " + shoppingDesc);

        //hide all canvas layer
        background.gameObject.SetActive(false);
        backgroundSM.gameObject.SetActive(false);
        PaymentBackgroundSM.gameObject.SetActive(false);
        t_PaymentBackground.gameObject.SetActive(false);
        btShoppingEndTE.gameObject.SetActive(false);
        btShoppingEndSM.gameObject.SetActive(false);
        //activate the shopping
        if (shoppingDesc == "smallmarket") {
            background.gameObject.SetActive(true);
        } else {

            backgroundSM.gameObject.SetActive(true);
        }
        // set mouse cursor on visible
        Cursor.visible = true;

    }

    // Selection of the favourite product
    public void OnClickProduct(string name) {

        //productcounter --> Count of Items
        //check the productcounter --> if it's < 3 u can choose an Item
        if (productCounter == 2) {
            if (shoppingDesc == "smallmarket") {
                //after choice ==> change to payment (TanteEmmaLaden)
                background.gameObject.SetActive(false);
                t_PaymentBackground.gameObject.SetActive(true);
            } else {
                //after choice ==> change to payment (SuperMarket)
                backgroundSM.gameObject.SetActive(false);
                PaymentBackgroundSM.gameObject.SetActive(true);
            }

        } else {
            //check which Item is choosed an deactivate him and his partner
            if (name == "bioMilchTE" || name == "normalMilchTE" || name == "bioMilchSM" || name == "normalMilchSM") {
                vd.SetShoppingProductMilk(name);
                normalMilkTE.gameObject.SetActive(false);
                bioMilkTE.gameObject.SetActive(false);
                normalMilkSM.gameObject.SetActive(false);
                bioMilkSM.gameObject.SetActive(false);
            }
            if (name == "bioAppleTE" || name == "normalAppleTE" || name == "bioAppleSM" || name == "normalAppleSM") {
                vd.SetShoppingProductApple(name);
                normalAppleTE.gameObject.SetActive(false);
                bioAppleTE.gameObject.SetActive(false);
                normalAppleSM.gameObject.SetActive(false);
                bioAppleSM.gameObject.SetActive(false);
            }
            if (name == "cerealBarTE" || name == "chocolatBarTE" || name == "cerealBarSM" || name == "chocolatBarSM") {
                vd.SetShoppingProductBar(name);
                cerealBarTE.gameObject.SetActive(false);
                chocolatBarTE.gameObject.SetActive(false);
                cerealBarSM.gameObject.SetActive(false);
                chocolatBarSM.gameObject.SetActive(false);
            }
            //productcounter +1
            productCounter++;
            //activate the End button
            if ( shoppingDesc == "smallmarket" ) {
                btShoppingEndTE.gameObject.SetActive(true);
            } else {
                btShoppingEndSM.gameObject.SetActive(true);
            }

            //print("gewählt " + productCounter + vd.GetShoppingProductMilk() + vd.GetShoppingProductApple() + vd.GetShoppingProductBar());
        }
    }
    //choice of payment 
    public void OnClickPayment(string name) {
        vd.SetShoppingDecision(name);
        //load samplescene
        sceneTrigger.scene = 1;
        sceneTrigger.changeScene();            
    }
    public void OnClickEnd() {
        //end of shopping --> goes to payment
        if (shoppingDesc == "smallmarket") {

            background.gameObject.SetActive(false);
            t_PaymentBackground.gameObject.SetActive(true);

        }
        if (shoppingDesc == "largemarket") {

            backgroundSM.gameObject.SetActive(false);
            PaymentBackgroundSM.gameObject.SetActive(true);

        }

    }

}

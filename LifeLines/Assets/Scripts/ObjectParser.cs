﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectParser : MonoBehaviour
{
    ValueDump vd;
    public GameObject apple, choco, familyCar, offroader, sportsCar, laptop, smartphone, letter, you;
    string food, car, kommdevice, loc, genderpartner, children;
    public PicHandler picHandler;
    public NumberPlate numberPlate;
    public FamilyPic familyPic;
    private void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();

        Persistence();


    }

    public void Persistence()
    {
        food = vd.GetFood();
        Debug.Log("FOOD: " + vd.GetFood());
        car = vd.GetCar();
        kommdevice = vd.GetKommdevice();
        loc = vd.GetLocation();
        picHandler.vd = this.vd;
        picHandler.GetPicture();
        familyPic.vd = this.vd;
        string gender = vd.GetGenderChar();
        string inputpartner = vd.GetGenderPartner();
        string inputchildren = vd.GetChildren();
        Debug.Log("Familienpic: " + gender + " " + inputpartner + " " + inputchildren);
        familyPic.DisplayPicParsed();


        //Umbau zu SWITCH-CASE
        //default-state beachten!
        //switch(food.Equals) {
        //  case ....
        if (food.Equals("Apple"))
        {
            apple.SetActive(false);
            choco.GetComponent<BoxCollider>().enabled = false;

        }
        else if (food.Equals("Choco"))
        {
            choco.SetActive(false);
            apple.GetComponent<BoxCollider>().enabled = false;

        }

        if (car.Equals("Familycar"))
        {
            familyCar.SetActive(true);
            numberPlate.ParseMyPlate(car, loc);
        }
        else if (car.Equals("Offroader"))
        {
            offroader.SetActive(true);
            numberPlate.ParseMyPlate(car, loc);
        }
        else if (car.Equals("Sportscar"))
        {
            sportsCar.SetActive(true);
            numberPlate.ParseMyPlate(car, loc);
        }

        if (kommdevice.Equals("Laptop"))
        {
            laptop.SetActive(false);
            smartphone.GetComponent<BoxCollider>().enabled = false;
            letter.GetComponent<BoxCollider>().enabled = false;
        }
        else if (kommdevice.Equals("Smartphone"))
        {
            smartphone.SetActive(false);
            laptop.GetComponent<BoxCollider>().enabled = false;
            letter.GetComponent<BoxCollider>().enabled = false;
        }
        else if (kommdevice.Equals("Letter"))
        {
            letter.SetActive(false);
            smartphone.GetComponent<BoxCollider>().enabled = false;
            laptop.GetComponent<BoxCollider>().enabled = false;
        }



    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionMain : MonoBehaviour
{   
    //deprecated; not used in this context anymore
    //was used in a collider in order to activate the picture selection on screen

    //set up
    public GameObject bilder, ph;
    PicHandler picHandler;


    /*private void OnCollisionEnter(Collision collision)
    {   
        Debug.Log("Collision mit iwas" );
    }*/

    private void Start()
    {   
        //grab the pichandler
        picHandler = ph.GetComponent<PicHandler>();
    }
    private void OnTriggerExit(Collider other)
    {   
        //if you leave the triggerarea hide the pic selection
        Debug.Log("Im outta here: " + other.ToString());
        bilder.SetActive(false);
    }
    private void OnTriggerEnter(Collider other1)
    {   
        //if you enter delete the pic that was previously chosen and make the pic selection visible
        picHandler.DelPic();
        bilder.SetActive(true);
    }

    
}

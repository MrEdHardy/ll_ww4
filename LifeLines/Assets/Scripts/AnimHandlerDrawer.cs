﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimHandlerDrawer : MonoBehaviour
{   
    //author ~~Jonathan
    //not used; just for debug
    
    public Animator anim;
    public Raycast player;
    ValueDump vd;

    public int doit;
    bool isOpen;

    private void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
        player = GetComponent<Raycast>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        doit = vd.GetSideDoorStatus();
        if (doit == 1)
        {
            vd.SetStatusSideDoor(0);
            Checkstatus();
        }

    }

    public void Checkstatus()
    {
        if (isOpen == true)
        {
            string sss = gameObject.name;
            Debug.Log("Ich schließe was");
            Debug.Log("name: " + sss);
            Close();
            isOpen = false;

        }
        else
        {
            Debug.Log("Ich öffne was");
            Open();
            isOpen = true;
        }

        
        
    }


    public void Open()
    {
        anim.SetBool("isOpen", true);
    }

    public void Close()
    {
        anim.SetBool("isOpen", false);
    }
}

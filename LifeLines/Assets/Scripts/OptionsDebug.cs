﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsDebug : MonoBehaviour
{   
    //used for displaying the optionsmenu
    //author ~~Jonathan

    public GameObject player;
    public CameraScript cs;
    GameObject value;
    public ValueDump vd;
    bool troll;
    private void Awake()
    {
        /*value = GameObject.FindGameObjectWithTag("ValueDump");
        if (value != null)
        {

            vd = value.GetComponent<ValueDump>();
            if (vd != null)
            {
                Debug.Log("ValueDump gefunden");
                troll = vd.GetInteractable();
                Debug.Log("trrr:" + troll);
            }
        }
        else
        {
            Debug.Log("Error");
        }*/
    }
    private void Start()
    {
        //player = GameObject.FindGameObjectWithTag("Player");
        //rc = player.GetComponentInChildren<Raycast>();
        //value = GameObject.FindGameObjectWithTag("ValueDump");
        /*if (value != null)
        {
            
            vd = value.GetComponent<ValueDump>();
            if(vd != null)
            {
                Debug.Log("ValueDump gefunden");
            }
        }
        else
        {
            Debug.Log("Error");
        }*/
    }

    //show the optionsmenu and disable the player and camera movement
    public void ShowOptions()
    {
        DisableScripts();
        Debug.Log("Ich werde angezeigt");
        this.gameObject.SetActive(true);
        
    }

    //disable the optionsmenu and enable the scripts
    public void CloseOptions()
    {
        Debug.Log("Ich schließe mich");
        this.gameObject.SetActive(false);
        EnableScripts();
    }

    //disable scripts
    public void DisableScripts()
    {
        //player.GetComponent<PlayerScript>().enabled = false;
        //cs.enabled = false;
        Debug.Log("Interactable: " + vd.GetInteractable());
        vd.SetInteractable(false);
    }

    //enable scripts
    public void EnableScripts()
    {
        //player.GetComponent<PlayerScript>().enabled = true;
        //cs.enabled = true;
        
        if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
        {
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
        }
        vd.SetInteractable(true);
    }



}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ValueDump : MonoBehaviour
{   
    //used to store variables across scenes; contains getter and setter methods 
    //author ~~Jonathan
    static private int pic = 0;
    static int statusDrawer;
    static private int statusDoorMain;
    static int sidedoorstatus, entrydoorstatus, entrydoorfrontstatus;
    static bool interacted;
    static bool interactable = true;
    static string kommdevice = "", infodevice= "", food = "", shopping= "";
    static string shoppingDecision = "", shoppingProductMilk = "", shoppingProductApple = "", shoppingProductBar = "";
    static string jobless = "", jobselection= "";
    static string car = "",propulsion = "";
    static string bike = "";
    static string loc ="";
    static string profession = "";
    static string degree = "";
    static string career = "";
    static string gender_partner = "", children = "";
    static string genderchar = "male";

    static private List<Dictionary<string, string>> dics= new List<Dictionary<string, string>>();
    static Dictionary<string, string> contextdic = new Dictionary<string, string>();
    static Dictionary<string, string> questlogdic = new Dictionary<string, string>();
    static List<string> questlogkeys = new List<string>();

    public List<string> GetQuestLogKeys()
    {
        return questlogkeys;
    }

    public void SetQuestLogKeys(List<string> questlogkeys)
    {
        ValueDump.questlogkeys = questlogkeys;
    }

    public Dictionary<string,string> GetQuestLogDic()
    {
        return questlogdic;
    }

    public void SetQuestLogDic(Dictionary<string, string> questlogdic)
    {
        ValueDump.questlogdic = questlogdic;
    }

    public string GetGenderChar()
    {
        return genderchar;
    }

    public void SetGenderChar(string genderchar)
    {
        ValueDump.genderchar = genderchar;
    }

    public string GetChildren()
    {
        return children;
    }

    public void SetChildren(string children)
    {
        ValueDump.children = children;
    }

    public string GetGenderPartner()
    {
        return gender_partner;
    }

    public void SetGenderPartner(string gender)
    {
        ValueDump.gender_partner = gender;
    }

    public string GetCareer()
    {
        return career;
    }

    public void SetCareer(string career)
    {
        ValueDump.career = career;
    }

    public string GetDegree()
    {
        return degree;
    }

    public void SetDegree(string degree)
    {
        ValueDump.degree = degree;
    }

    public string GetProfession()
    {
        return profession;
    }

    public void SetProfession(string profession)
    {
        ValueDump.profession = profession;
    }

    public bool GetInteractable()
    {
        return interactable;
    }

    public void SetInteractable(bool interactable)
    {
        ValueDump.interactable = interactable;
    }

    public string GetLocation()
    {
        return loc;
    }

    public void SetLoc(string loc)
    {
        ValueDump.loc = loc;
    }

    public string GetBike()
    {
        return bike;
    }

    public void SetBike(string bike)
    {
        ValueDump.bike = bike;
    }

    public string GetPropulsion()
    {
        return propulsion;
    }

    public void SetPropulsion(string propulsion)
    {
        ValueDump.propulsion = propulsion;
    }

    public string GetCar()
    {
        return car;
    }

    public void SetCar(string carselection)
    {
        ValueDump.car = carselection;
    }

    public string GetJobselection()
    {
        return jobselection;
    }

    public void SetJobselection(string jobselection)
    {
        ValueDump.jobselection = jobselection;
    }

    public string GetJobless()
    {
        return jobless;
    }

    public void SetJobless(string jobless)
    {
        ValueDump.jobless = jobless;
    }

    public string GetKommdevice()
    {
        return kommdevice;
    }

    public void SetKommDevice(string kommdevice)
    {
        ValueDump.kommdevice = kommdevice;
    }

    public string GetInfodevice()
    {
        return infodevice;
    }

    public void SetInfodevice(string infodevice)
    {
        ValueDump.infodevice = infodevice;
    }

    public string GetFood()
    {
        return food;
    }

    public void SetFood(string food)
    {
        ValueDump.food = food;
    }

    public bool GetInteracted()
    {
        return interacted;
    }

    public void SetInteracted(bool interacted)
    {
        ValueDump.interacted = interacted;
    }

    public int GetPic()
    {
        return pic;
    }

    public void SetPic(int pic)
    {
        ValueDump.pic = pic;
    }

    public void AddDics(Dictionary<string, string> dic)
    {
        dics.Add(dic);

    }

    public List<Dictionary<string,string>> GetDics()
    {
        return dics;
    }

    public int GetSceneIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public int GetEntrydoorfrontstatus()
    {
        return entrydoorfrontstatus;
    }

    public void SetEntrydoorfrontstatus(int entrydoorfrontstatus)
    {
        ValueDump.entrydoorfrontstatus = entrydoorfrontstatus;
    }

    public int GetStatusMainDoor()
    {
        return statusDoorMain;
    }
    
    public void SetStatusMainDoor(int statusmaindoor)
    {
        ValueDump.statusDoorMain = statusmaindoor;
    }

    public int GetSideDoorStatus()
    {
        return sidedoorstatus;
    }

    public void SetStatusSideDoor(int sidedoorstatus)
    {
        ValueDump.sidedoorstatus = sidedoorstatus;
    }

    public int GetEntryDoorStatus()
    {
        return entrydoorstatus;
    }

    public void SetEntryDoorStatus(int entrydoorstatus)
    {
        ValueDump.entrydoorstatus = entrydoorstatus;
    }
    public int GetStatusPCDrawer1()
    {
        return statusDrawer;
    }

    public void SetStatusPCDrawer1(int statuspcdrawer1)
    {
        ValueDump.statusDrawer = statuspcdrawer1;
    }

    public Dictionary<string, string> GetContextTexts()
    {
        return contextdic;
    }

    public void SetContextTexts(Dictionary<string, string> contextdic)
    {
        ValueDump.contextdic = contextdic;
    }

    public string GetShopping() {
        return shopping;
    }

    public void SetShopping(string shopping) {
        ValueDump.shopping = shopping;
    }

    public string GetShoppingProductMilk() {
        return shoppingProductMilk;
    }

    public void SetShoppingProductMilk(string shoppingProductMilk) {
        ValueDump.shoppingProductMilk = shoppingProductMilk;
    }

    public string GetShoppingProductApple() {
        return shoppingProductApple;
    }

    public void SetShoppingProductApple(string shoppingProductApple) {
        ValueDump.shoppingProductApple = shoppingProductApple;
    }

    public string GetShoppingProductBar() {
        return shoppingProductBar;
    }

    public void SetShoppingProductBar(string shoppingProductBar) {
        ValueDump.shoppingProductBar = shoppingProductBar;
    }

    public string GetShoppingDecision() {
        return shoppingDecision;
    }

    public void SetShoppingDecision(string shoppingDecision) {
        ValueDump.shoppingDecision = shoppingDecision;
    }
}

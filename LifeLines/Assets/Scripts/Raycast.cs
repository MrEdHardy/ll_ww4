﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Raycast : MonoBehaviour
{   
    //used to handle the raycast
    //author: TimF, Jonathan, Marcel 
    private GameObject raycastetObject; //object the raycast hit

    Dictionary<string, string> dicc; //dic context; contains the context texts 

    [SerializeField] private int rayLenght = 2; // raylenght
    [SerializeField] private LayerMask layerMaskInteract; // special layer used to interact with the raycast

    [SerializeField] private Image uiCrosshair;
    [SerializeField] private List<Text> uiText;

   
    ValueDump vd;
    public GameObject bilder, apfel, schokolade,handy, sandwichObject, bananaObject,briefumschlag,laptop, pc, newspaper;
    public GameObject carpic1, carpic2, carpic3,familyCar,offRoader,sportsCar,ec,hc,cc, propulsion, frame, bikey, bikeG, numberplateObject, familypicObject;
    public GameObject bilderrahmen1; // small picframe 1
    public GameObject bilderrahmen2; // small picframe 2
    public GameObject bilderrahmen3; // small picframe
    public GameObject shopTE, shopSM;
    public Image interactTextBg; // Background for the interaction Text
    
    //Set up
    SceneTrigger sceneTriggerPC,sceneTriggerNP, sceneTriggerShoppingTE, sceneTriggerShoppingSM;
    DecisionHandler smartphone,personalcomputer,letter, portablepc,apple,choco,np, sandwich, banana, shopDecisionTE, shopDecisionSM;
    DecisionHandler pic1, pic2, pic3,electro,hybrid,combustion; //carpic, propulsion dec handler vars
    DecisionHandler bike;
    DecisionHandler plate;
    //Text text1;
    //public int selection;
    DeletePic delpic;
    //UIText uiText;
    //bool pressed;
    NumberPlate numberPlate;
    FamilyPic familyPic;
    public QuestLogHandler qlh;

    static GameObject lastinteracted;

    private void Start()
    {
        //uiText = GetComponent<UIText>();
        
        smartphone = GameObject.FindGameObjectWithTag("Handy").GetComponent<DecisionHandler>();
        personalcomputer = GameObject.FindGameObjectWithTag("PC").GetComponent<DecisionHandler>();
        letter = GameObject.FindGameObjectWithTag("Briefumschlag").GetComponent<DecisionHandler>();
        portablepc = GameObject.FindGameObjectWithTag("Laptop").GetComponent<DecisionHandler>();
        apple = GameObject.FindGameObjectWithTag("Apfel").GetComponent<DecisionHandler>();
        choco = GameObject.FindGameObjectWithTag("Schokolade").GetComponent<DecisionHandler>();
        //np = newspaper
        np = GameObject.FindGameObjectWithTag("Zeitung").GetComponent<DecisionHandler>();
        shopDecisionTE = GameObject.FindGameObjectWithTag("Door_TE").GetComponent<DecisionHandler>();
        shopDecisionSM = GameObject.FindGameObjectWithTag("Door_SM").GetComponent<DecisionHandler>();

        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
        delpic = GetComponent<DeletePic>();
        pc = GameObject.FindGameObjectWithTag("PC");
        newspaper = GameObject.FindGameObjectWithTag("Zeitung");
        shopTE = GameObject.FindGameObjectWithTag("Door_TE");
        shopSM = GameObject.FindGameObjectWithTag("Door_SM");
        //Debug.Log("Liste: " + uiText.Count);
        sceneTriggerPC = pc.GetComponent<SceneTrigger>();
        sceneTriggerNP = newspaper.GetComponent<SceneTrigger>();
        sceneTriggerShoppingTE = shopTE.GetComponent<SceneTrigger>();
        sceneTriggerShoppingSM = shopSM.GetComponent<SceneTrigger>();

        pic1 = carpic1.GetComponent<DecisionHandler>();
        pic2 = carpic2.GetComponent<DecisionHandler>();
        pic3 = carpic3.GetComponent<DecisionHandler>();

        electro = ec.GetComponent<DecisionHandler>();
        hybrid = hc.GetComponent<DecisionHandler>();
        combustion = cc.GetComponent<DecisionHandler>();

        bike = bikey.GetComponent<DecisionHandler>();
        

        plate = numberplateObject.GetComponent<DecisionHandler>();
        numberPlate = numberplateObject.GetComponent<NumberPlate>();
        familyPic = familypicObject.GetComponent<FamilyPic>();

        sandwich = sandwichObject.GetComponent<DecisionHandler>();
        banana = bananaObject.GetComponent<DecisionHandler>();

        dicc = vd.GetContextTexts();
        if(dicc == null)
        {
            Debug.Log("Fail");
        }
        else
        {
            //Debug.Log("lelelele" + dicc["door"].ToString());
        }
    }

    private void Update()
    {
        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward); //lokale Var fwd (forward) ist forward der Kamera

        //Raycast in current forward position with raylenght and if something is hit
        if (Physics.Raycast(transform.position, fwd, out hit, rayLenght, layerMaskInteract.value)) {

            // is something hit with the tag 'Tuer'?
            if (hit.collider.CompareTag("Tuer")) {
                //set the hit object
                raycastetObject = hit.collider.gameObject;
                //change crosshair color
                CrosshairActive();

                interactTextBg.enabled = true;
                //enable and set context Text
                uiText[1].enabled = true;
                //uiText[1].text = "Tür öffnen";
                uiText[1].text = dicc["door"];

                if (Input.GetKeyDown("e")) {
                    Debug.Log("i have interacted with Tür");

                    //set the status of the maindoor
                    vd.SetStatusMainDoor(1);


                    //raycastetObject.SetActive(false);


                }


            }

            if (hit.collider.CompareTag("Handy")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                Debug.Log("i see a handy!");
                uiText[2].enabled = true;
                //uiText[2].text = "chatten";
                uiText[2].text = dicc["smartphone"];
                if (Input.GetKeyDown("e")) {

                    Debug.Log("i have interacted with Handy!");
                    //make decision that is defined on object
                    smartphone.MakeDecision();

                    lastinteracted = raycastetObject;
                    qlh.CompleteQuest(1);

                    //disable the interacted object
                    raycastetObject.SetActive(false);

                    //make the other object uninteractable by disabling their collider
                    laptop.GetComponent<BoxCollider>().enabled = false;
                    briefumschlag.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if (hit.collider.CompareTag("Schublade")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[3].enabled = true;
                //uiText[3].text = "Schublade öffnen";
                uiText[3].text = dicc["drawer"];

                //check if another text instance is active
                if (uiText[11].IsActive() == true) {
                    uiText[11].enabled = false;
                }
                if (uiText[9].IsActive() == true) {
                    uiText[9].enabled = false;
                }
                if (Input.GetKeyDown("e")) {
                    vd.SetStatusPCDrawer1(1);

                }
            }

            if (hit.collider.CompareTag("Seitentuer")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[1].enabled = true;
                //uiText[1].text = "Tür öffnen";
                uiText[1].text = dicc["door"];
                if (Input.GetKeyDown("e")) {
                    vd.SetStatusSideDoor(1);
                    Debug.Log("i have interacted with Seitentür!");
                    // raycastetObject.SetActive(false);
                    //uiText.text = "Das ist ein Handy";
                }
            }

            if (hit.collider.CompareTag("Eingangstuer")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[1].enabled = true;
                //uiText[1].text = "Tür öffnen";
                uiText[1].text = dicc["door"];
                if (Input.GetKeyDown("e")) {
                    vd.SetEntryDoorStatus(1);
                    Debug.Log("i have interacted with Eingangstür!");
                    // raycastetObject.SetActive(false);
                    //uiText.text = "Das ist ein Handy";
                }
            }


            if (hit.collider.CompareTag("EntranceDoorFront")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[1].enabled = true;
                //uiText[1].text = "Tür öffnen";
                uiText[1].text = dicc["door"];
                if (Input.GetKeyDown("e")) {
                    vd.SetEntrydoorfrontstatus(1);
                    Debug.Log("i have interacted with Eingangstür!");
                    // raycastetObject.SetActive(false);
                    //uiText.text = "Das ist ein Handy";
                }
            }

            if (hit.collider.CompareTag("minipic")) {
                raycastetObject = hit.collider.gameObject; // getroffenens Objekt setzen

                CrosshairActive(); // crosshair aender, fals man auf ein onjekt zum interagieren zeigt
                interactTextBg.enabled = true;
                uiText[4].enabled = true;
                //uiText[4].text = "Bild auswählen";
                uiText[4].text = dicc["minipic"];

                if (uiText[5].IsActive() == true) {
                    uiText[5].enabled = false;
                }

                if (Input.GetKeyDown("e")) {
                    if (raycastetObject.name.Equals("Bilderrahmen (1)")) {
                        //int selection = 1;
                        vd.SetPic(1);
                        vd.SetInteracted(true);

                        //if the big frame is activated by key pressing disable the small pic frames
                        bilderrahmen1.SetActive(false);
                        bilderrahmen2.SetActive(false);
                        bilderrahmen3.SetActive(false);

                        qlh.CompleteQuest(4);

                        Debug.Log("i have interacted with Pic");
                        Debug.Log("Name: " + raycastetObject.name);
                    } else if (raycastetObject.name.Equals("Bilderrahmen (2)")) {
                        vd.SetPic(2);
                        vd.SetInteracted(true);
                        //Instantiate(pics[2]);
                        bilderrahmen1.SetActive(false);
                        bilderrahmen2.SetActive(false);
                        bilderrahmen3.SetActive(false);

                        qlh.CompleteQuest(4);
                        Debug.Log("i have interacted with Pic");
                        Debug.Log("Name: " + raycastetObject.name);
                    } else {
                        vd.SetPic(3);
                        vd.SetInteracted(true);
                        bilderrahmen1.SetActive(false);
                        bilderrahmen2.SetActive(false);
                        bilderrahmen3.SetActive(false);

                        qlh.CompleteQuest(4);
                        Debug.Log("i have interacted with Pic");
                        Debug.Log("Name: " + raycastetObject.name);
                    }
                    // raycastetObject.SetActive(false);
                    //uiText.text = "Das ist ein Handy";
                }
            }

            if (hit.collider.CompareTag("Poster")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[5].enabled = true;
                //uiText[5].text = "Poster aufhängen";
                uiText[5].text = dicc["poster"];
                if (uiText[4].IsActive() == true) {
                    uiText[4].enabled = false;
                }

                if (Input.GetKeyDown("e")) {

                    Debug.Log("i have interacted with Poster!");
                    //delete the pic if you interact with the pic frame
                    delpic.DelPic();
                    //set the small pics true
                    bilder.SetActive(true);
                    bilderrahmen1.SetActive(true);
                    bilderrahmen2.SetActive(true);
                    bilderrahmen3.SetActive(true);
                    // raycastetObject.SetActive(false);
                    //uiText.text = "Das ist ein Handy";
                }
            }

            if (hit.collider.CompareTag("Apfel")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[6].enabled = true;
                //uiText[6].text = "Apfel essen";
                uiText[6].text = dicc["apple"];



                if (uiText[7].IsActive() == true) {
                    uiText[7].enabled = false;
                }
                if (Input.GetKeyDown("e")) {
                    Debug.Log("i have interacted with Apfel!");
                    apple.MakeDecision();
                    raycastetObject.SetActive(false);

                    sandwichObject.GetComponent<BoxCollider>().enabled = false;
                    bananaObject.GetComponent<BoxCollider>().enabled = false;
                    schokolade.GetComponent<BoxCollider>().enabled = false;

                    lastinteracted = raycastetObject;
                    qlh.CompleteQuest(3);
                }
            }

            if (hit.collider.CompareTag("Schokolade")) {
                raycastetObject = hit.collider.gameObject; // getroffenens Objekt setzen
                CrosshairActive(); // crosshair aender, fals man auf ein onjekt zum interagieren zeigt
                interactTextBg.enabled = true;
                uiText[7].enabled = true;
                //uiText[7].text = "Schokolade essen";
                uiText[7].text = dicc["choco"];

                if (uiText[6].IsActive() == true) {
                    uiText[6].enabled = false;
                }
                if (Input.GetKeyDown("e")) {

                    choco.MakeDecision();
                    raycastetObject.SetActive(false);

                    apfel.GetComponent<BoxCollider>().enabled = false;
                    bananaObject.GetComponent<BoxCollider>().enabled = false;
                    sandwichObject.GetComponent<BoxCollider>().enabled = false;

                    lastinteracted = raycastetObject;
                    qlh.CompleteQuest(3);
                }

            }

            if (hit.collider.CompareTag("Laptop")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                Debug.Log("i a Laptop!");
                uiText[8].enabled = true;
                interactTextBg.enabled = true;
                //uiText[8].text = "Mail schreiben";
                uiText[8].text = dicc["laptop"];
                if (Input.GetKeyDown("e")) {
                    portablepc.MakeDecision();
                    raycastetObject.SetActive(false);
                    handy.GetComponent<BoxCollider>().enabled = false;
                    briefumschlag.GetComponent<BoxCollider>().enabled = false;

                    lastinteracted = raycastetObject;
                    qlh.CompleteQuest(1);
                }
            }

            if (hit.collider.CompareTag("Briefumschlag")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                Debug.Log("i see a Briefumschlag!");
                uiText[9].enabled = true;
                //uiText[9].text = "Brief schreiben";
                uiText[9].text = dicc["letter"];
                if (uiText[3].IsActive() == true) {
                    uiText[3].enabled = false;
                }
                if (uiText[11].IsActive() == true) {
                    uiText[11].enabled = false;
                }

                if (Input.GetKeyDown("e")) {
                    letter.MakeDecision();
                    raycastetObject.SetActive(false);
                    handy.GetComponent<BoxCollider>().enabled = false;
                    laptop.GetComponent<BoxCollider>().enabled = false;

                    lastinteracted = raycastetObject;
                    qlh.CompleteQuest(1);


                }
            }

            if (hit.collider.CompareTag("PC")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                Debug.Log("i see a PC!");
                uiText[10].enabled = true;
                //uiText[10].text = "online nach Job suchen";
                uiText[10].text = dicc["pc"];
                if (Input.GetKeyDown("e")) {
                    personalcomputer.MakeDecision();
                    newspaper.GetComponent<BoxCollider>().enabled = false;
                    //raycastetObject.SetActive(false);

                    //change scene based on a script that is attached to the object
                    sceneTriggerPC.changeScene();

                }
            }

            if (hit.collider.CompareTag("Zeitung")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                Debug.Log("i see a newspaper!");
                uiText[11].enabled = true;
                //uiText[11].text = "in Zeitung nach Job suchen";
                uiText[11].text = dicc["newspaper"];

                if (uiText[3].IsActive() == true) {
                    uiText[3].enabled = false;
                }
                if (uiText[9].IsActive() == true) {
                    uiText[9].enabled = false;
                }
                if (Input.GetKeyDown("e")) {
                    np.MakeDecision();
                    pc.GetComponent<BoxCollider>().enabled = false;
                    //raycastetObject.SetActive(false);
                    sceneTriggerNP.changeScene();
                }
            }

            //controlls the car pic selection
            if (hit.collider.CompareTag("Carpic")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //Debug.Log("Ich sehe was");
                if (uiText[13].IsActive() == true) {
                    uiText[13].enabled = false;
                }

                //uiText[12].text = "Familienwagen wählen";
                uiText[12].text = dicc["familycar"];
                uiText[12].enabled = true;

                if (Input.GetKeyDown("e")) {
                    Debug.Log("Erstes Auto");
                    pic1.MakeDecision();
                    frame.SetActive(false);
                    propulsion.SetActive(true);
                    sportsCar.SetActive(false);
                    offRoader.SetActive(false);
                    familyCar.SetActive(false);

                }

            }

            if (hit.collider.CompareTag("Carpic2")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //Debug.Log("Ich sehe was");
                if (uiText[12].IsActive() == true) {
                    uiText[12].enabled = false;
                }

                //uiText[13].text = "Geländewagen wählen";
                uiText[13].text = dicc["offroader"];
                uiText[13].enabled = true;

                if (Input.GetKeyDown("e")) {
                    Debug.Log("Zweites Auto");
                    pic2.MakeDecision();
                    frame.SetActive(false);
                    propulsion.SetActive(true);
                    offRoader.SetActive(false);
                    sportsCar.SetActive(false);
                    familyCar.SetActive(false);


                }

            }

            if (hit.collider.CompareTag("Carpic3")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //Debug.Log("Ich sehe was");
                //uiText[14].text = "Sportwagen wählen";
                uiText[14].text = dicc["sportscar"];
                uiText[14].enabled = true;

                if (Input.GetKeyDown("e")) {
                    Debug.Log("Drittes Auto");
                    pic3.MakeDecision();
                    frame.SetActive(false);
                    propulsion.SetActive(true);
                    sportsCar.SetActive(false);
                    offRoader.SetActive(false);
                    familyCar.SetActive(false);

                }

            }

            //choosing the propulsion type
            if (hit.collider.CompareTag("Electro")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //Debug.Log("Ich sehe was");

                if (uiText[13].IsActive() == true || uiText[14].IsActive() == true || uiText[12].IsActive() == true) {
                    uiText[13].enabled = false;
                    uiText[14].enabled = false;
                    uiText[12].enabled = false;
                }

                //uiText[15].text = "Antrieb wählen";
                uiText[15].text = dicc["propulsion"];
                uiText[15].enabled = true;

                if (Input.GetKeyDown("e")) {
                    Debug.Log("Electro");
                    electro.MakeDecision();
                    propulsion.SetActive(false);
                    frame.SetActive(true);
                    string car = vd.GetCar();

                    if (car.Equals("Familycar")) {
                        familyCar.SetActive(true);
                    } else if (car.Equals("Offroader")) {
                        offRoader.SetActive(true);
                    } else {
                        sportsCar.SetActive(true);
                    }
                    uiText[15].enabled = false;
                    qlh.CompleteQuest(5);


                }

            }

            if (hit.collider.CompareTag("Hybrid")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //Debug.Log("Ich sehe was");

                if (uiText[13].IsActive() == true || uiText[14].IsActive() == true || uiText[12].IsActive() == true) {
                    uiText[13].enabled = false;
                    uiText[14].enabled = false;
                    uiText[12].enabled = false;
                }

                //uiText[15].text = "Antrieb wählen";
                uiText[15].text = dicc["propulsion"];
                uiText[15].enabled = true;

                if (Input.GetKeyDown("e")) {
                    Debug.Log("Hybrid");
                    hybrid.MakeDecision();
                    propulsion.SetActive(false);
                    frame.SetActive(true);
                    string car = vd.GetCar();

                    if (car.Equals("Familycar")) {
                        familyCar.SetActive(true);
                    } else if (car.Equals("Offroader")) {
                        offRoader.SetActive(true);
                    } else {
                        sportsCar.SetActive(true);
                    }
                    uiText[15].enabled = false;
                    qlh.CompleteQuest(5);


                }

            }

            if (hit.collider.CompareTag("Combustion")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //Debug.Log("Ich sehe was");

                if (uiText[13].IsActive() == true || uiText[14].IsActive() == true || uiText[12].IsActive() == true) {
                    uiText[13].enabled = false;
                    uiText[14].enabled = false;
                    uiText[12].enabled = false;
                }

                //uiText[15].text = "Antrieb wählen";
                uiText[15].text = dicc["propulsion"];
                uiText[15].enabled = true;

                if (Input.GetKeyDown("e")) {
                    Debug.Log("Combustion");
                    combustion.MakeDecision();
                    propulsion.SetActive(false);
                    frame.SetActive(true);
                    string car = vd.GetCar();

                    if (car.Equals("Familycar")) {
                        familyCar.SetActive(true);
                    } else if (car.Equals("Offroader")) {
                        offRoader.SetActive(true);
                    } else {
                        sportsCar.SetActive(true);
                    }
                    uiText[15].enabled = false;
                    qlh.CompleteQuest(5);


                }

            }

            if (hit.collider.CompareTag("Bike")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //Debug.Log("Ich sehe was");

                //uiText[16].text = "Fahrrad wählen";
                uiText[16].text = dicc["bike"];
                uiText[16].enabled = true;

                if (Input.GetKeyDown("e")) {
                    Debug.Log("Bike");
                    bike.MakeDecision();
                    bikeG.SetActive(true);
                    bikey.SetActive(false);





                }

            }

            if (hit.collider.CompareTag("NumberPlate")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[16].enabled = true;
                uiText[16].text = dicc["numberplate"];

                if (Input.GetKeyDown("e")) {
                    numberPlate.MakeMeVisible(raycastetObject, vd);
                }

            }

            if (hit.collider.CompareTag("FamilyPic")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[16].enabled = true;
                uiText[16].text = dicc["familypic"];

                if (Input.GetKeyDown("e")) {
                    familyPic.MakeMeVisible(vd);
                }
            }

            if (hit.collider.CompareTag("Sandwichstuff")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[16].enabled = true;
                uiText[16].text = dicc["sandwich"];

                if (Input.GetKeyDown("e")) {
                    sandwich.MakeDecision();
                    raycastetObject.SetActive(false);
                    //sandwichObject.GetComponentInChildren<Toggle>().hideMe();
                    apfel.GetComponent<BoxCollider>().enabled = false;
                    bananaObject.GetComponent<BoxCollider>().enabled = false;
                    schokolade.GetComponent<BoxCollider>().enabled = false;

                    lastinteracted = raycastetObject;
                    qlh.CompleteQuest(3);
                }
            }

            if (hit.collider.CompareTag("Banana")) {
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                uiText[16].enabled = true;
                uiText[16].text = dicc["apple"];

                if (Input.GetKeyDown("e")) {
                    banana.MakeDecision();
                    raycastetObject.SetActive(false);
                    //sandwichObject.GetComponentInChildren<Toggle>().hideMe();
                    apfel.GetComponent<BoxCollider>().enabled = false;
                    sandwichObject.GetComponent<BoxCollider>().enabled = false;
                    schokolade.GetComponent<BoxCollider>().enabled = false;

                    lastinteracted = raycastetObject;
                    qlh.CompleteQuest(3);
                }
            }

            // do i hover above the door for the small shop?
            if (hit.collider.CompareTag("Door_TE")) {

                //get collider
                raycastetObject = hit.collider.gameObject;
                CrosshairActive();
                interactTextBg.enabled = true;
                //enable and set context Text
                uiText[1].enabled = true;
                //uiText[1].text = "Tür öffnen";
                uiText[1].text = dicc["door"];

                //is button e pressed on the keyboard?
                if (Input.GetKeyDown("e")) {
                    print("TE");

                    //set shop-decision
                    shopDecisionTE.MakeDecision();
                    //disable box-collider
                    shopTE.GetComponent<BoxCollider>().enabled = false;
                    //raycastetObject.SetActive(false);
                    //change the scene to scene 3
                    //scene 3 -> shopping-scene
                    sceneTriggerShoppingTE.changeScene();
                }
            }
                // do i hover above the door for the small shop?
                if (hit.collider.CompareTag("Door_SM")) {
                    print("Door_SM");
                    //get collider
                    raycastetObject = hit.collider.gameObject;
                    CrosshairActive();
                    interactTextBg.enabled = true; 
                    //enable and set context Text
                    uiText[1].enabled = true;
                    //uiText[1].text = "Tür öffnen";
                    uiText[1].text = dicc["door"];

                    //is button e pressed on the keyboard?
                    if (Input.GetKeyDown("e")) {
                        print("SM");

                        //set shop-decision
                        shopDecisionSM.MakeDecision();
                        //disable box-collider
                        shopSM.GetComponent<BoxCollider>().enabled = false;
                        //raycastetObject.SetActive(false);
                        //change the scene to scene 3
                        //scene 3 -> shopping-scene
                        sceneTriggerShoppingSM.changeScene();
                    }
                }

            
        } else {
            //normal state of the crosshair
            CrosshairNormal();
            numberPlate.HideMe();
            familyPic.HideMe();

            interactTextBg.enabled = false;
            uiText[0].text = "";

            //disable all the Context Texts //zukunft iwan mal als for oder foreach schleife!!
            uiText[1].enabled = false;
            uiText[2].enabled = false;
            uiText[3].enabled = false;
            uiText[4].enabled = false;
            uiText[5].enabled = false;
            uiText[6].enabled = false;
            uiText[7].enabled = false;
            uiText[8].enabled = false;
            uiText[9].enabled = false;
            uiText[10].enabled = false;
            uiText[11].enabled = false;
            uiText[12].enabled = false;
            uiText[13].enabled = false;
            uiText[14].enabled = false;
            uiText[15].enabled = false;
            uiText[16].enabled = false;
        }
    }

    void CrosshairActive()
    {
        uiCrosshair.color = Color.red;
    }
    void CrosshairNormal()
    {

        uiCrosshair.color = Color.white;
    }
    public void ResetDecisions()
    {   
        if(lastinteracted != null)
        {
            if (lastinteracted.GetComponent<DecisionHandler>().theme.Equals("Kommdevice"))
            {
                laptop.SetActive(true);
                briefumschlag.SetActive(true);
                handy.SetActive(true);

                laptop.GetComponent<BoxCollider>().enabled = true;
                briefumschlag.GetComponent<BoxCollider>().enabled = true;
                handy.GetComponent<BoxCollider>().enabled = true;

                qlh.ReverseDecision();

                
            }
            else if (lastinteracted.GetComponent<DecisionHandler>().theme.Equals("Food"))
            {
                sandwichObject.SetActive(true);
                schokolade.SetActive(true);
                apfel.SetActive(true);
                bananaObject.SetActive(true);

                bananaObject.GetComponent<BoxCollider>().enabled = true;
                sandwichObject.GetComponent<BoxCollider>().enabled = true;
                schokolade.GetComponent<BoxCollider>().enabled = true;
                apfel.GetComponent<BoxCollider>().enabled = true;

                qlh.ReverseDecision();

                
            }
            
        }
        else
        {
            Debug.Log("Fail");
        }

        lastinteracted = null;
        
    }
}

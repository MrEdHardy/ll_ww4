﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeletePic : MonoBehaviour
{   
    //used to reset the chosen picture from the large pic frame
    //author ~~Jonathan

    //set up
    GameObject[] picts;
    public void DelPic()
    {   
        //fill the array with all gameobjects with this tag
        picts = GameObject.FindGameObjectsWithTag("Pic");
        Debug.Log("ich versuche zu löschen");
        Debug.Log(picts.ToString());

        //check if the array is empty
        if (picts != null)
        {
            Debug.Log("Array ist leer");
            //foreach element in picts delete it
            foreach (GameObject pic in picts)
            {
                Destroy(pic);
            }

        }
    }
}

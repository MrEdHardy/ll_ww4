﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    //used to handle the player movements
    //author ~~Jonathan
    public GameObject options;
    OptionsDebug opt;
    bool interactable;
    GameObject value;
    ValueDump vd;
    [SerializeField]
    float speed = 4;
    
    // Start is called before the first frame update
    void Start()
    {

        //Cursor.lockState = CursorLockMode.Locked;
        //check for scene; Make cursor visible or not
        Debug.Log("Szene: " + SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name.Equals("Scene2"))
        {
            Cursor.visible = true;
        }
        else if (SceneManager.GetActiveScene().name.Equals("Scene3"))
        {
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
        }
        
        //options = GameObject.FindGameObjectWithTag("Options");
        opt = options.GetComponent<OptionsDebug>();

        value = GameObject.FindGameObjectWithTag("ValueDump");
        if (value != null)
        {   
            
            vd = value.GetComponent<ValueDump>();
        }
        else
        {
            Debug.Log("Error");
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
        interactable = vd.GetInteractable();
        
        if(interactable == true)
        {   
            //movement
            float vert = Input.GetAxis("Vertical") * speed *Time.deltaTime;
            float hor = Input.GetAxis("Horizontal") * speed* Time.deltaTime;

            transform.Translate(hor, 0 , vert);

            //when escape is pressed show options and make the cursor visible 
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //Cursor.lockState = CursorLockMode.None;
                Debug.Log("Ich tue was");
                Debug.Log("Playerint: " + vd.GetInteractable());
                opt.vd = this.vd;
                opt.ShowOptions();
                Cursor.visible = true;
                
            
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                Raycast rc = GetComponentInChildren<Raycast>();
                rc.ResetDecisions();
            }

            if (SceneManager.GetActiveScene().name.Equals("Scene2"))
            {
                Cursor.visible = true;
            }
            else if (SceneManager.GetActiveScene().name.Equals("Scene3"))
            {
                Cursor.visible = true;
            }
        }
        
    }
}

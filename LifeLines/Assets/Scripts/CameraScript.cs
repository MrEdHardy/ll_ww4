﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{   
    //used to make the camera move
    //author ~~Jonathan

    //Vectors and variables for the Lookaround and the smoothing to make it look more natural
    Vector2 mouseLook;
    Vector2 smoothV;
    bool interactable;
    public float sensivity = 5f;
    public float smoothing = 2f;
    ValueDump vd;

    public Quaternion Anfang;
    public Quaternion Ende;


    //max value for looking upwards
    [SerializeField]
    private int elevationmax = 40;

    //max value for looking down
    [SerializeField]
    private int depressionmax = -45;

    //the player
    GameObject character;
    private void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
        //this script sits on the main camera, which is a child of the player; grab the player gameobject
        character = this.transform.parent.gameObject;
        
    }

    private void Update()
    {


        interactable = vd.GetInteractable();

        if (interactable == true)
        {
            //mouse delta, get x,y coords from mouse coords
            var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            //vector multiplication between the mouse coords and a smoothing component
            md = Vector2.Scale(md, new Vector2(sensivity * smoothing, sensivity * smoothing));

            //smooth the mouse vector
            smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
            smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
            //set the smoothed vector on the mouselook
            mouseLook += smoothV;
            //Debug Stuff
            //Debug.Log(mouseLook.ToString());

            //set the yaw and pitch limits

            if (mouseLook.y >= elevationmax)
            {
                mouseLook.y = elevationmax;
                transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
            }

            else
            {
                transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
            }

            if (mouseLook.y <= depressionmax)
            {
                mouseLook.y = depressionmax;
                transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
            }
            else
            {
                transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
            }

            Anfang = transform.rotation;
        }
        else
        {
            Ende = transform.rotation;
            float deltaFickDich = Ende.x - Anfang.x;
            if (deltaFickDich != 0)
            {
                Rigidbody player = gameObject.GetComponentInParent<Rigidbody>();
                player.angularVelocity = Vector3.zero;
                //Debug.Log("FEHLERHANDLING rotation bei kennzeichen ausgeführt");
            }
        }
       
        
    }
    }

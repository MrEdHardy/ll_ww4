﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FamilyPic : MonoBehaviour
{
    public Dropdown partnerDropdown;
    public Dropdown childrenDropdown;

    public GameObject interactionMenu;
    public GameObject cursor;
    //public GameObject[] partner, children;
    GameObject[] cleanup;
    public ValueDump vd;
    public QuestLogHandler qlh;
    public GameObject[] familyPics;

    public int inputpartner, inputchildren;
    static int partner, child, character;
    private void Update()
    {
        
    }
    public void MakeMeVisible(ValueDump valueDump)
    {
        Cursor.visible = true;
        interactionMenu.SetActive(true);
        DisableEverything();

        vd = valueDump;
        vd.SetInteractable(false);
        cursor.SetActive(false);
    }

    public void PartnerInput()
    {
        inputpartner = partnerDropdown.value;
        partner = inputpartner;

        if(inputpartner == 0)
        {
            Debug.Log("kein Partner");
            vd.SetGenderPartner("no partner");
        }
        else if(inputpartner == 1)
        {
            
            vd.SetGenderPartner("female");
        }
        else if(inputpartner == 2)
        {
            
            vd.SetGenderPartner("male");
        }
        else if(inputpartner == 3)
        {
            Debug.Log("Divers");
            vd.SetGenderPartner("diverse");
        }
        
    }

    public void PartnerInputParsed()
    {
        string input = vd.GetGenderPartner();

        if (input.Equals("no partner"))
        {
            Debug.Log("kein Partner");
            
        }
        else if (input.Equals("female"))
        {
            
            
        }
        else if (input.Equals("male"))
        {
            
            
        }
        else if(input.Equals("diverse"))
        {
            Debug.Log("Divers");
          
        }
        
    }

    public void ChildrenInput()
    {   
        
        inputchildren = childrenDropdown.value;
        child = inputchildren;
        if (inputchildren == 0)
        {
            
            vd.SetChildren("no children");
            Debug.Log("Auswahl: " + vd.GetChildren());
        }
        else if (inputchildren == 1)
        {
            
            vd.SetChildren("1");
            Debug.Log("Auswahl: " + vd.GetChildren());
        }
        else if (inputchildren == 2)
        {
            
            vd.SetChildren("2");
            Debug.Log("Auswahl: " + vd.GetChildren());
        }
        else if (inputchildren == 3)
        {
            
            vd.SetChildren("3");
            Debug.Log("Auswahl: " + vd.GetChildren());
        }
        else if(inputchildren == 4)
        {
            
            vd.SetChildren("4");
            Debug.Log("Auswahl: " + vd.GetChildren());
        }
        else
        {
            
            vd.SetChildren("more than four");
            Debug.Log("Auswahl: " + vd.GetChildren());
        }

        

    }

    public void ChildrenInputParsed()
    {
        string input = vd.GetChildren();
        if (input.Equals("0"))
        {

            
            vd.SetChildren("no children");
        }
        else if (input.Equals("1"))
        {
            
            Debug.Log("Auswahl: " + vd.GetChildren());
            
        }
        else if (input.Equals("2"))
        {
            
            Debug.Log("Auswahl: " + vd.GetChildren());
            
        }
        else if (input.Equals("3"))
        {
            
            Debug.Log("Auswahl: " + vd.GetChildren());
           
        }
        else if (input.Equals("4"))
        {
            
            
            Debug.Log("Auswahl: " + vd.GetChildren());
        }
        else if (input.Equals("more than four"))
        {
            
            Debug.Log("Auswahl: " + vd.GetChildren());
            
        }

    }

    public void DisplayPic()
    {
        string gender = vd.GetGenderChar();

        if (gender.Equals("male"))
        {
            if(inputpartner == 0 && inputchildren == 0)
            {
                Instantiate(familyPics[3]);
                qlh.CompleteQuest(2);
            }
            else if(inputpartner == 0 && inputchildren == 1)
            {
                Instantiate(familyPics[4]);
                qlh.CompleteQuest(2);
            }
            else if(inputpartner == 0 && inputchildren == 2)
            {
                Instantiate(familyPics[5]);
                qlh.CompleteQuest(2);
            }
            else if(inputpartner == 1 && inputchildren == 0)
            {
                Instantiate(familyPics[2]);
                qlh.CompleteQuest(2);
            }
            else if(inputpartner == 1 && inputchildren == 1)
            {
                Instantiate(familyPics[1]);
                qlh.CompleteQuest(2);
            }
            else if (inputpartner == 1 && inputchildren == 2)
            {
                Instantiate(familyPics[0]);
                qlh.CompleteQuest(2);
            }
        }
        else
        {
            if (inputpartner == 0 && inputchildren == 0)
            {
                Instantiate(familyPics[6]);
                qlh.CompleteQuest(2);
            }
            else if(inputpartner == 0 && inputchildren == 1)
            {
                Instantiate(familyPics[7]);
                qlh.CompleteQuest(2);
            }
            else if(inputpartner == 0 && inputchildren == 2)
            {
                Instantiate(familyPics[8]);
                qlh.CompleteQuest(2);
            }
            else if (inputpartner == 2 && inputchildren == 0)
            {
                Instantiate(familyPics[2]);
                qlh.CompleteQuest(2);
            }
            else if (inputpartner == 2 && inputchildren == 1)
            {
                Instantiate(familyPics[1]);
                qlh.CompleteQuest(2);
            }
            else if(inputpartner == 2 && inputchildren == 2)
            {
                Instantiate(familyPics[0]);
                qlh.CompleteQuest(2);
            }
        }
    }

    public void DisplayPicParsed()
    {
        string gender = vd.GetGenderChar();


        if (gender.Equals("male"))
        {
            if (partner == 0 && child == 0)
            {
                
                Instantiate(familyPics[3]);
            }
            else if (partner == 0 && child == 1)
            {
                Instantiate(familyPics[4]);
            }
            else if (partner == 0 && child == 2)
            {
                Instantiate(familyPics[5]);
            }
            else if (partner == 1 && child == 0)
            {
                Instantiate(familyPics[2]);
            }
            else if (partner == 1 && child == 1)
            {
                Instantiate(familyPics[1]);
            }
            else if (partner == 1 && child == 2)
            {
                Instantiate(familyPics[0]);
            }
        }
        else
        {
            if (partner == 0 && child == 0)
            {
                Instantiate(familyPics[6]);
            }
            else if (partner == 0 && child == 1)
            {
                Instantiate(familyPics[7]);
            }
            else if (partner == 0 && child == 2)
            {
                Instantiate(familyPics[8]);
            }
            else if (partner == 2 && child == 0)
            {
                Instantiate(familyPics[2]);
            }
            else if (partner == 2 && child == 1)
            {
                Instantiate(familyPics[1]);
            }
            else if (partner == 2 && child == 2)
            {
                Instantiate(familyPics[0]);
            }
        }
    }

    public void DisableEverything()
    {
        
        cleanup = GameObject.FindGameObjectsWithTag("Children");
        
        /*for(int i = 0; i< partner.Length; i++)
        {
            partner[i].SetActive(false);
        }*/

        foreach(GameObject go in cleanup)
        {
            Destroy(go);
        }
    }

    public void HideMe()
    {
        Cursor.visible = false;
        interactionMenu.SetActive(false);
    }

    public void EnableScripts()
    {
        vd.SetInteractable(true);
        cursor.SetActive(true);
    }


}

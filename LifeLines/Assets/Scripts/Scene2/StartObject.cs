﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartObject : MonoBehaviour
{
    public GameObject CanvasObject;

    void Start()
    {
        CanvasObject.SetActive(true);
    }

}

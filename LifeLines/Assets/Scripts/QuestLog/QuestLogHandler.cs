﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuestLogHandler : MonoBehaviour
{
    ValueDump vd;
    public Text QuestLogText;
    public GameObject pc, newspaper, drawer, smartphone, laptop, picframe, banana, apple, choco, sandwich, poster, carpic1, carpic2, carpic3, familycar, offroader, sportscar, numberplate_familycar, numberplate_offroader, numberplate_sportscar, bike, smallstore, largestore, largestore2;
    Dictionary<string, string> questlogdic = new Dictionary<string, string>();
    List<string> questlogkeys = new List<string>();

    static bool[] questactive = new bool[8];
    static int i = 0;

    private void Start()
    {   
        
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
        if(vd == null)
        {
            Debug.Log("HI");
        }
        else
        {
            Debug.Log("not null");
        }
        
        


        questlogkeys = vd.GetQuestLogKeys();
        questlogdic = vd.GetQuestLogDic();
        
    }

    private void Update()
    {
        //Debug.Log("vd questindex: " + vd.GetQuestIndex());

        QuestLog();
    }

    public void QuestLog()
    {
        if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
        {
            //UMBAU ZU SWITCH-CASE
            //default-state beachten!
            if (i == 0)
            {
                questactive[i] = true;
                //Debug.Log("questchanger :" + questactive[i]);
                string aa = questlogdic[questlogkeys[i]];
                //Debug.Log("aa: " + aa);
                QuestLogText.text = questlogdic[questlogkeys[i]];

                pc.GetComponent<Toggle>().showMe();
                newspaper.GetComponent<Toggle>().showMe();
                
            }
            else if (i == 1)
            {
                
                questactive[i - 1] = false;
                questactive[i] = true;
                picframe.GetComponentInChildren<Toggle>().hideMe();
                pc.GetComponent<Toggle>().hideMe();
                newspaper.GetComponent<Toggle>().hideMe();

                QuestLogText.text = questlogdic[questlogkeys[i]];

                drawer.GetComponent<Toggle>().showMe();
                smartphone.GetComponent<Toggle>().showMe();
                laptop.GetComponent<Toggle>().showMe();

                questactive[i] = true;

            }
            else if (i == 2)
            {
                questactive[i - 1] = false;
                questactive[i] = true;
                drawer.GetComponent<Toggle>().hideMe();
                smartphone.GetComponent<Toggle>().hideMe();
                laptop.GetComponent<Toggle>().hideMe();

                QuestLogText.text = questlogdic[questlogkeys[i]];

                picframe.GetComponentInChildren<Toggle>().showMe();

            }
            else if (i == 3)
            {
                questactive[i - 1] = false;
                questactive[i] = true;
                picframe.GetComponentInChildren<Toggle>().hideMe();
                poster.GetComponent<Toggle>().hideMe();
                QuestLogText.text = questlogdic[questlogkeys[i]];

                banana.GetComponentInChildren<Toggle>().showMe();
                apple.GetComponent<Toggle>().showMe();
                choco.GetComponent<Toggle>().showMe();
                sandwich.GetComponentInChildren<Toggle>().showMe();
            }
            else if (i == 4)
            {
                questactive[i - 1] = false;
                questactive[i] = true;
                banana.GetComponentInChildren<Toggle>().hideMe();
                apple.GetComponent<Toggle>().hideMe();
                choco.GetComponent<Toggle>().hideMe();
                sandwich.GetComponentInChildren<Toggle>().hideMe();

                QuestLogText.text = questlogdic[questlogkeys[i]];

                poster.GetComponent<Toggle>().showMe();
            }
            else if (i == 5)
            {
                questactive[i - 1] = false;
                questactive[i] = true;
                poster.GetComponent<Toggle>().hideMe();

                QuestLogText.text = questlogdic[questlogkeys[i]];

                carpic1.GetComponent<Toggle>().showMe();
                carpic2.GetComponent<Toggle>().showMe();
                carpic3.GetComponent<Toggle>().showMe();
                bike.GetComponent<Toggle>().showMe();
            }
            else if (i == 6)
            {
                questactive[i - 1] = false;
                questactive[i] = true;
                carpic1.GetComponent<Toggle>().hideMe();
                carpic2.GetComponent<Toggle>().hideMe();
                carpic3.GetComponent<Toggle>().hideMe();
                bike.GetComponent<Toggle>().hideMe();

                QuestLogText.text = questlogdic[questlogkeys[i]];



                if (familycar.activeSelf == true)
                {
                    numberplate_familycar.GetComponentInChildren<Toggle>().showMe();
                }
                else if (offroader.activeSelf == true)
                {
                    numberplate_offroader.GetComponentInChildren<Toggle>().showMe();
                }
                else if (sportscar.activeSelf == true)
                {
                    numberplate_sportscar.GetComponentInChildren<Toggle>().showMe();
                }

            }
            else if (i == 7)
            {
                questactive[i - 1] = false;
                questactive[i] = true;
                numberplate_familycar.GetComponentInChildren<Toggle>().hideMe();
                numberplate_offroader.GetComponentInChildren<Toggle>().hideMe();
                numberplate_sportscar.GetComponentInChildren<Toggle>().hideMe();

                QuestLogText.text = questlogdic[questlogkeys[i]];

                smallstore.GetComponentInChildren<Toggle>().showMe();
                largestore.GetComponentInChildren<Toggle>().showMe();
            }
        }
        


    }

    public void CompleteQuest(int index)
    {   
        if(questactive[index] == true)
        {
            i = i + 1;
            Debug.Log("it works");
        }
        else
        {
            Debug.Log("sry quest is noch nich dran");
            Debug.Log("Debug questactive " + questactive[index] + "index: " + i );
        }
    }

    public void ReverseDecision()
    {
        questactive[i] = false;
        i = i - 1;

    }
}

﻿using cakeslice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toggle : MonoBehaviour
{
    private void Start()
    {
        hideMe();
    }

    // Start is called before the first frame update
    public void hideMe()
    {
        GetComponent<Outline>().enabled = false;
    }

    public void showMe()
    {
        GetComponent<Outline>().enabled = true;
    }
}

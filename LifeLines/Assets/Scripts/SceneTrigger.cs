﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTrigger : MonoBehaviour
{   
    //used to change scene 
    //author ~~Jonathan

    
    public int scene;

    public void changeScene()
    {
        SceneManager.LoadScene(scene);
    }
}

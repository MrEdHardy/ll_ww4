﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharEdit : MonoBehaviour {
    internal float value;
    //Age and Skin Men
    public Canvas menDarkOld, menDarkMid, menDarkYoung, menMidOld, menMidMid, menMidYoung, menBrightOld, menBrightMid, menBrightYoung;
    //ClothesMen
    public Canvas hairSmoothBlond, hairSmoothBrown, hairSmoothBlack, hairShoulderBlond, hairShoulderBrown, hairShoulderBlack, hairWavesBlond,
                    hairWavesBrown, hairWavesBlack, topShirtBlue, topShirtGray, topLongBlue, topLongRed, topShortBlue, topShortRed, pantsShortBlue,
                    pantsShortGray, pantsLongBlue, pantsLongGray, footShortBlue, footShortGray, footLongBlue, footLongGray;

    //Age and Skin Men
    public Canvas femDarkOld, femDarkMid, femDarkYoung, femBrightOld, femBrightMid, femBrightYoung, femMidOld, femMidMid, femMidYoung;

    //ClothesFemale
    public Canvas femHairShortBlond, femHairShortBrown, femHairShortBlack, femHairLongBlond, femHairLongBrown, femHairLongBlack, femHairMidBlond, 
                    femHairMidBrown, femHairMidBlack, femTopShirtBlue, femTopShirtGray, femTopLongBlue, femTopLongRed, femTopShortBlue, 
                    femTopShortRed, femPantsShortBlue, femPantsShortRed, femPantsLongBlue, femPantsLongRed, femPantsSkirtBlue, femPantsSkirtRed, 
                    femFootBallGray, femFootBallRed, femFootOpen, femFootSneakGray, femFootSneakRed;

    public Canvas symbolMale, symbolFemale;
    public Canvas maleSkin, femaleSkin, femaleDress, maleDress;

    public Canvas backgroundChar, backgroundHobby;

    private float sliderAgeValue, sliderSkinValue;
    
    private int arrowCounterHairMen = 0, arrowCounterTopMen = 0, arrowCounterPantsMen = 0, arrowCounterFootMen = 0, arrowCounterHairFem = 0, arrowCounterTopFem = 0, arrowCounterPantsFem = 0, arrowCounterFootFem = 0;
    private bool GenderMale = true;
    private bool GenderFemale = false;

    SceneTrigger sceneTrigger = new SceneTrigger();

    // Start is called before the first frame update
    void Start() {
        Cursor.visible = true;
        

        symbolMale.gameObject.SetActive(true);
        symbolFemale.gameObject.SetActive(true);
        hairSmoothBlond.gameObject.SetActive(true);
        topShirtBlue.gameObject.SetActive(true);
        pantsShortBlue.gameObject.SetActive(true);
        footShortBlue.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update() {
        //Debug.Log(sliderAgeValue);
        
        if (GenderMale == true) {
            femaleSkin.gameObject.SetActive(false);
            maleSkin.gameObject.SetActive(true);
            if (sliderAgeValue <= 35) {
                if (sliderSkinValue == 0) {
                menDarkYoung.gameObject.SetActive(false);
                menDarkMid.gameObject.SetActive(false);
                menDarkOld.gameObject.SetActive(false);
                menBrightYoung.gameObject.SetActive(true);
                menBrightMid.gameObject.SetActive(false);
                menBrightOld.gameObject.SetActive(false);
                menMidYoung.gameObject.SetActive(false);
                menMidMid.gameObject.SetActive(false);
                menMidOld.gameObject.SetActive(false);
                } else
                if (sliderSkinValue == 1) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(true);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);

                } else
                if (sliderSkinValue == 2) {
                    menDarkYoung.gameObject.SetActive(true);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                }

            } else if (sliderAgeValue > 35 && sliderAgeValue <= 50) {
                if (sliderSkinValue == 0) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(true);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                } else
                if (sliderSkinValue == 1) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(true);
                    menMidOld.gameObject.SetActive(false);

                } else
                if (sliderSkinValue == 2) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(true);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                }
            } else if (sliderAgeValue > 50) {
                if (sliderSkinValue == 0) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(true);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                } else
                if (sliderSkinValue == 1) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(true);

                } else
                if (sliderSkinValue == 2) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(true);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                }
            }
        }
        if (GenderFemale == true) {
            maleSkin.gameObject.SetActive(false);
            femaleSkin.gameObject.SetActive(true);
            if (sliderAgeValue <= 35) {
                if (sliderSkinValue == 0) {
                    Debug.Log("is loaded");
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(true);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                } else
                if (sliderSkinValue == 1) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(true);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);

                } else
                if (sliderSkinValue == 2) {
                    femDarkYoung.gameObject.SetActive(true);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                }

            } else if (sliderAgeValue > 35 && sliderAgeValue <= 50) {
                if (sliderSkinValue == 0) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(true);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                } else
                if (sliderSkinValue == 1) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(true);
                    femMidOld.gameObject.SetActive(false);
               
                } else
                if (sliderSkinValue == 2) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(true);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                }
            } else if (sliderAgeValue > 50) {
                if (sliderSkinValue == 0) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(true);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                } else
                if (sliderSkinValue == 1) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(true);

                } else
                if (sliderSkinValue == 2) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(true);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                }
            }
        }
    }

    public void SliderSkin(float valueSkin) {
        // 0 = young, 1 = midold, 2 = old
        Debug.Log(valueSkin);
        sliderSkinValue = valueSkin;

    }

    public void SliderAge(float valueAge) {
        //value from 4-12
        //value *5 for 20-60 years
        valueAge = valueAge * 5;
        Debug.Log(valueAge);
        sliderAgeValue = valueAge;

    }

    public void OnClickArrow(String name) {

        Debug.Log(name);
        if (GenderMale == true) { 
        
            hairSmoothBlond.gameObject.SetActive(false);
            hairSmoothBrown.gameObject.SetActive(false);
            hairSmoothBlack.gameObject.SetActive(false);
            hairShoulderBlond.gameObject.SetActive(false);
            hairShoulderBrown.gameObject.SetActive(false);
            hairShoulderBlack.gameObject.SetActive(false);
            hairWavesBlond.gameObject.SetActive(false);
            hairWavesBrown.gameObject.SetActive(false);
            hairWavesBlack.gameObject.SetActive(false);

            topShirtBlue.gameObject.SetActive(false);
            topShirtGray.gameObject.SetActive(false);
            topLongBlue.gameObject.SetActive(false);
            topLongRed.gameObject.SetActive(false);
            topShortBlue.gameObject.SetActive(false);
            topShortRed.gameObject.SetActive(false);

            pantsShortBlue.gameObject.SetActive(false);
            pantsShortGray.gameObject.SetActive(false);
            pantsLongBlue.gameObject.SetActive(false);
            pantsLongGray.gameObject.SetActive(false);

            footShortBlue.gameObject.SetActive(false);
            footShortGray.gameObject.SetActive(false);
            footLongBlue.gameObject.SetActive(false);
            footLongGray.gameObject.SetActive(false);

            if (name == "arrowlefthair") {

                arrowCounterHairMen--;

                if (arrowCounterHairMen == -1) {

                    arrowCounterHairMen = 8;

                }
            }
            if (name == "arrowrighthair") {

                arrowCounterHairMen++;
                if (arrowCounterHairMen == 9) {
                    arrowCounterHairMen = 0;
                }

            }

            switch (arrowCounterHairMen) {

                case 0:
                    hairSmoothBlond.gameObject.SetActive(true);
                    break;
                case 1:
                    hairSmoothBrown.gameObject.SetActive(true);
                    break;
                case 2:
                    hairSmoothBlack.gameObject.SetActive(true);
                    break;
                case 3:
                    hairShoulderBlond.gameObject.SetActive(true);
                    break;
                case 4:
                    hairShoulderBrown.gameObject.SetActive(true);
                    break;
                case 5:
                    hairShoulderBlack.gameObject.SetActive(true);
                    break;
                case 6:
                    hairWavesBlond.gameObject.SetActive(true);
                    break;
                case 7:
                    hairWavesBrown.gameObject.SetActive(true);
                    break;
                case 8:
                    hairWavesBlack.gameObject.SetActive(true);
                    break;

            }
            if (name == "arrowlefttop") {

                arrowCounterTopMen--;

                if (arrowCounterTopMen == -1) {

                    arrowCounterTopMen = 5;

                }
            }
            if (name == "arrowrighttop") {

                arrowCounterTopMen++;
                if (arrowCounterTopMen == 6) {
                    arrowCounterTopMen = 0;
                }

            }

            switch (arrowCounterTopMen) {

                case 0:
                    topShirtBlue.gameObject.SetActive(true);
                    break;
                case 1:
                    topShirtGray.gameObject.SetActive(true);
                    break;
                case 2:
                    topLongBlue.gameObject.SetActive(true);
                    break;
                case 3:
                    topLongRed.gameObject.SetActive(true);
                    break;
                case 4:
                    topShortBlue.gameObject.SetActive(true);
                    break;
                case 5:
                    topShortRed.gameObject.SetActive(true);
                    break;
            }

            if (name == "arrowleftpants") {

                arrowCounterPantsMen--;

                if (arrowCounterPantsMen == -1) {

                    arrowCounterPantsMen = 3;

                }
            }
            if (name == "arrowrightpants") {

                arrowCounterPantsMen++;
                if (arrowCounterPantsMen == 4) {
                    arrowCounterPantsMen = 0;
                }

            }

            switch (arrowCounterPantsMen) {

                case 0:
                    pantsShortBlue.gameObject.SetActive(true);
                    break;
                case 1:
                    pantsShortGray.gameObject.SetActive(true);
                    break;
                case 2:
                    pantsLongBlue.gameObject.SetActive(true);
                    break;
                case 3:
                    pantsLongGray.gameObject.SetActive(true);
                    break;
            }

            if (name == "arrowleftshoes") {

                arrowCounterFootMen--;

                if (arrowCounterFootMen == -1) {

                    arrowCounterFootMen = 1;

                }
            }
            if (name == "arrowrightshoes") {

                arrowCounterFootMen++;
                if (arrowCounterFootMen == 2) {
                    arrowCounterFootMen = 0;
                }

            }


            if (arrowCounterPantsMen == 0 || arrowCounterPantsMen == 1) {

                switch (arrowCounterFootMen) {

                    case 0:
                        footShortBlue.gameObject.SetActive(true);
                        break;
                    case 1:
                        footShortGray.gameObject.SetActive(true);
                        break;
                }

            }
            if (arrowCounterPantsMen == 2 || arrowCounterPantsMen == 3) {

                switch (arrowCounterFootMen) {

                    case 0:
                        footLongBlue.gameObject.SetActive(true);
                        break;
                    case 1:
                        footLongGray.gameObject.SetActive(true);
                        break;
                }
            }
        }
        if (GenderFemale == true) {

            femHairShortBlond.gameObject.SetActive(false);
            femHairShortBrown.gameObject.SetActive(false);
            femHairShortBlack.gameObject.SetActive(false);
            femHairLongBlond.gameObject.SetActive(false);
            femHairLongBrown.gameObject.SetActive(false);
            femHairLongBlack.gameObject.SetActive(false);
            femHairMidBlond.gameObject.SetActive(false);
            femHairMidBrown.gameObject.SetActive(false);
            femHairMidBlack.gameObject.SetActive(false);

            femTopShirtBlue.gameObject.SetActive(false);
            femTopShirtGray.gameObject.SetActive(false);
            femTopLongBlue.gameObject.SetActive(false);
            femTopLongRed.gameObject.SetActive(false);
            femTopShortBlue.gameObject.SetActive(false);
            femTopShortRed.gameObject.SetActive(false);

            femPantsShortBlue.gameObject.SetActive(false);
            femPantsShortRed.gameObject.SetActive(false);
            femPantsLongBlue.gameObject.SetActive(false);
            femPantsLongRed.gameObject.SetActive(false);
            femPantsSkirtBlue.gameObject.SetActive(false);
            femPantsSkirtRed.gameObject.SetActive(false);

            femFootBallGray.gameObject.SetActive(false);
            femFootBallRed.gameObject.SetActive(false);
            femFootOpen.gameObject.SetActive(false);
            femFootSneakGray.gameObject.SetActive(false);
            femFootSneakRed.gameObject.SetActive(false);


            if (name == "arrowlefthair") {

                arrowCounterHairFem--;

                if (arrowCounterHairFem == -1) {

                    arrowCounterHairFem = 8;

                }
            }
            if (name == "arrowrighthair") {

                arrowCounterHairFem++;
                if (arrowCounterHairFem == 9) {
                    arrowCounterHairFem = 0;
                }

            }
            switch (arrowCounterHairFem) {

                case 0:
                    femHairShortBlond.gameObject.SetActive(true);
                    break;
                case 1:
                    femHairShortBrown.gameObject.SetActive(true);
                    break;
                case 2:
                    femHairShortBlack.gameObject.SetActive(true);
                    break;
                case 3:
                    femHairLongBlond.gameObject.SetActive(true);
                    break;
                case 4:
                    femHairLongBrown.gameObject.SetActive(true);
                    break;
                case 5:
                    femHairLongBlack.gameObject.SetActive(true);
                    break;
                case 6:
                    femHairMidBlond.gameObject.SetActive(true);
                    break;
                case 7:
                    femHairMidBrown.gameObject.SetActive(true);
                    break;
                case 8:
                    femHairMidBlack.gameObject.SetActive(true);
                    break;
            }

            if (name == "arrowlefttop") {

                arrowCounterTopFem--;

                if (arrowCounterTopFem == -1) {

                    arrowCounterTopFem = 5;

                }
            }
            if (name == "arrowrighttop") {

                arrowCounterTopFem++;
                if (arrowCounterTopFem == 6) {
                    arrowCounterTopFem = 0;
                }

            }

            switch (arrowCounterTopFem) {

                case 0:
                    femTopShirtBlue.gameObject.SetActive(true);
                    break;
                case 1:
                    femTopShirtGray.gameObject.SetActive(true);
                    break;
                case 2:
                    femTopLongBlue.gameObject.SetActive(true);
                    break;
                case 3:
                    femTopLongRed.gameObject.SetActive(true);
                    break;
                case 4:
                    femTopShortBlue.gameObject.SetActive(true);
                    break;
                case 5:
                    femTopShortRed.gameObject.SetActive(true);
                    break;
            }
            

            if (name == "arrowleftpants") {

                arrowCounterPantsFem--;

                if (arrowCounterPantsFem == -1) {

                    arrowCounterPantsFem = 5;

                }
            }
            if (name == "arrowrightpants") {

                arrowCounterPantsFem++;
                if (arrowCounterPantsFem == 6) {
                    arrowCounterPantsFem = 0;
                }

            }
            switch (arrowCounterPantsFem) {

                case 0:
                    femPantsShortBlue.gameObject.SetActive(true);
                    break;
                case 1:
                    femPantsShortRed.gameObject.SetActive(true);
                    break;
                case 2:
                    femPantsLongBlue.gameObject.SetActive(true);
                    break;
                case 3:
                    femPantsLongRed.gameObject.SetActive(true);
                    break;
                case 4:
                    femPantsSkirtBlue.gameObject.SetActive(true);
                    break;
                case 5:
                    femPantsSkirtRed.gameObject.SetActive(true);
                    break;
            }
            
            if (name == "arrowleftshoes") {

                arrowCounterFootFem--;

                if (arrowCounterFootFem == -1) {

                    arrowCounterFootFem = 4;

                }
            }
            if (name == "arrowrightshoes") {

                arrowCounterFootFem++;
                if (arrowCounterFootFem == 5) {
                    arrowCounterFootFem = 0;
                }

            }
            
            switch (arrowCounterFootFem) {

                case 0:
                    femFootBallGray.gameObject.SetActive(true);
                    break;
                case 1:
                    femFootBallRed.gameObject.SetActive(true);
                    break;
                case 2:
                    femFootOpen.gameObject.SetActive(true);
                    break;
                case 3:
                    femFootSneakGray.gameObject.SetActive(true);
                    break;
                case 4:
                    femFootSneakRed.gameObject.SetActive(true);
                    break;
            }

        }
        Debug.Log("TopMen" + arrowCounterTopMen);
        Debug.Log("HairMen" + arrowCounterHairMen);
        Debug.Log("PantsMen" + arrowCounterPantsMen);
        Debug.Log("ShoesMen" + arrowCounterFootMen);
    }

    public void OnClickGender(String name) {
        if (name == "female" || name == "male") {
            hairSmoothBlond.gameObject.SetActive(false);
            hairSmoothBrown.gameObject.SetActive(false);
            hairSmoothBlack.gameObject.SetActive(false);
            hairShoulderBlond.gameObject.SetActive(false);
            hairShoulderBrown.gameObject.SetActive(false);
            hairShoulderBlack.gameObject.SetActive(false);
            hairWavesBlond.gameObject.SetActive(false);
            hairWavesBrown.gameObject.SetActive(false);
            hairWavesBlack.gameObject.SetActive(false);

            topShirtBlue.gameObject.SetActive(false);
            topShirtGray.gameObject.SetActive(false);
            topLongBlue.gameObject.SetActive(false);
            topLongRed.gameObject.SetActive(false);
            topShortBlue.gameObject.SetActive(false);
            topShortRed.gameObject.SetActive(false);

            pantsShortBlue.gameObject.SetActive(false);
            pantsShortGray.gameObject.SetActive(false);
            pantsLongBlue.gameObject.SetActive(false);
            pantsLongGray.gameObject.SetActive(false);

            footShortBlue.gameObject.SetActive(false);
            footShortGray.gameObject.SetActive(false);
            footLongBlue.gameObject.SetActive(false);
            footLongGray.gameObject.SetActive(false);

            femHairShortBlond.gameObject.SetActive(false);
            femHairShortBrown.gameObject.SetActive(false);
            femHairShortBlack.gameObject.SetActive(false);
            femHairLongBlond.gameObject.SetActive(false);
            femHairLongBrown.gameObject.SetActive(false);
            femHairLongBlack.gameObject.SetActive(false);
            femHairMidBlond.gameObject.SetActive(false);
            femHairMidBrown.gameObject.SetActive(false);
            femHairMidBlack.gameObject.SetActive(false);

            femTopShirtBlue.gameObject.SetActive(false);
            femTopShirtGray.gameObject.SetActive(false);
            femTopLongBlue.gameObject.SetActive(false);
            femTopLongRed.gameObject.SetActive(false);
            femTopShortBlue.gameObject.SetActive(false);
            femTopShortRed.gameObject.SetActive(false);

            femPantsShortBlue.gameObject.SetActive(false);
            femPantsShortRed.gameObject.SetActive(false);
            femPantsLongBlue.gameObject.SetActive(false);
            femPantsLongRed.gameObject.SetActive(false);
            femPantsSkirtBlue.gameObject.SetActive(false);
            femPantsSkirtRed.gameObject.SetActive(false);

            femFootBallGray.gameObject.SetActive(false);
            femFootBallRed.gameObject.SetActive(false);
            femFootOpen.gameObject.SetActive(false);
            femFootSneakGray.gameObject.SetActive(false);
            femFootSneakRed.gameObject.SetActive(false);

        }
        if (name == "female") {
            GenderFemale = true;
            GenderMale = false;

            femaleDress.gameObject.SetActive(true);
            maleDress.gameObject.SetActive(false);

            femHairShortBlond.gameObject.SetActive(true);
            femTopShirtBlue.gameObject.SetActive(true);
            femPantsShortBlue.gameObject.SetActive(true);
            femFootBallGray.gameObject.SetActive(true);
            
            arrowCounterFootFem = 0;
            arrowCounterHairFem = 0;
            arrowCounterPantsFem = 0;
            arrowCounterTopFem = 0;

        }
        if (name == "male") {
            GenderFemale = false;
            GenderMale = true;
            femaleDress.gameObject.SetActive(false);
            maleDress.gameObject.SetActive(true);

            hairSmoothBlond.gameObject.SetActive(true);
            topShirtBlue.gameObject.SetActive(true);
            pantsShortBlue.gameObject.SetActive(true);
            footShortBlue.gameObject.SetActive(true);
            

            arrowCounterTopMen = 0;
            arrowCounterHairMen = 0;
            arrowCounterPantsMen = 0;
            arrowCounterFootMen = 0;
        }

    }


    public void OnClickNext() {

        backgroundChar.gameObject.SetActive(false);
        backgroundHobby.gameObject.SetActive(true);
    }

    public void OnClickEnd() {

        sceneTrigger.scene = 1;
        sceneTrigger.changeScene();
        //edit
        //experimental ~JonathanK.
        if(GenderMale == true)
        {
            ValueDump valueDump = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
            valueDump.SetGenderChar("male");
            Debug.Log("Gender: " + valueDump.GetGenderChar());
        }
        else
        {
            ValueDump valueDump = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
            valueDump.SetGenderChar("female");
            Debug.Log("Gender: " + valueDump.GetGenderChar());
        }
        

    }

}

    


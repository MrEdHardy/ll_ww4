﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimHandler : MonoBehaviour
{   
    //author ~~Jonathan K.
    //handles Animation states of each animation


    //Animator of the gameobject and the ValueDump
    Animator anim;
    //Raycast player;
    ValueDump vd;

    //Variables for management
    int doit;
    bool isOpen;
    string gameObjectTag, gameObjectName;

    private void Start()
    {   
        //grab the ValueDump
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();
        //player = GameObject.FindGameObjectWithTag("Player").GetComponent<Raycast>();

        //get the name Animator component and the gameobjects name and tag
        anim = GetComponent<Animator>();
        gameObjectTag = this.gameObject.tag;
        gameObjectName = this.gameObject.name;

    }

    private void Update()
    {   
        //checking the gameobjects tag
        if(gameObjectTag.Equals("Schublade"))
        {
            //get the status for this specific animated gameobject
            doit = vd.GetStatusPCDrawer1();
            if (doit == 1)
            {   
                //if set to true (1), set it back to 0 in valuedump and start animation 
                vd.SetStatusPCDrawer1(0);
                Checkstatus();
            }
        }
        else if(gameObjectTag.Equals("Tuer"))
        {
            
            doit = vd.GetStatusMainDoor();
            if (doit == 1)
            {
                vd.SetStatusMainDoor(0);
                Checkstatus();
            }

        }
        else if (gameObjectTag.Equals("Seitentuer"))
        {
            doit = vd.GetSideDoorStatus();
            
            if (doit == 1)
            {
                vd.SetStatusSideDoor(0);
                Checkstatus();
            }
        }
        else if (gameObjectTag.Equals("Eingangstuer"))
        {
            doit = vd.GetEntryDoorStatus();
            if (doit == 1)
            {
                vd.SetEntryDoorStatus(0);
                Checkstatus();
            }
        }
        else if (gameObjectTag.Equals("EntranceDoorFront"))
        {
            doit = vd.GetEntrydoorfrontstatus();
            if (doit == 1)
            {
                vd.SetEntrydoorfrontstatus(0);
                Checkstatus();
            }
        }
        
    
    }
    
    //check if open animation is already triggered, then play close animation otherwise play the open anim
    public void Checkstatus()
    {
        if (isOpen == true)
        {
            Debug.Log("Ich schließe was");
           
            Close();
            isOpen = false;

            }
        else
        {
            Debug.Log("Ich öffne was");
            Open();
            isOpen = true;
        }
            
     }
    
    //used to play the open anim
    public void Open()
    {
        anim.SetBool("isOpen", true);
    }

    //used to play the close anim
    public void Close()
    {
        anim.SetBool("isOpen", false);
    }
}

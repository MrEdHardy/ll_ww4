﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class settingsmenu : MonoBehaviour {
    //controlls the functionality of the optionsmenu
    //author ~~Jonathan

    public AudioMixer audiomixer;
    Resolution[] resolutions;
    public Dropdown resdrop, qualdrop;
    public Slider volume, sensivity;
    public CameraScript cs;
    private void Update()
    {
        Cursor.visible = true;
    }
    private void Start()
    {
        //get all supported resolutions
        //clear the dropdown
        resolutions = Screen.resolutions;
        resdrop.ClearOptions();

        List<string> opt = new List<string>();

        int currentresindex = 0;

        //go through the array and save the resolutions height and width in a list; also set the currentresindex to current resolution in the dropdown
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            opt.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentresindex = i;
            }
        }

        //add the list to the dropdown and choose the current resolution in the dropdown
        resdrop.AddOptions(opt);
        resdrop.value = currentresindex;
        resdrop.RefreshShownValue();

        float value;
        audiomixer.GetFloat("Volume", out value);
        
        qualdrop.value = QualitySettings.GetQualityLevel();
        volume.value = value;
        sensivity.value = cs.sensivity;
        
    }

    //set volume
    public void setvolume(float volume)
    {
        Debug.Log("Volume: " + volume);
        audiomixer.SetFloat("Volume", volume);
       
    }

    //set unity quality preset
    public void setquality(int quality)
    {
        QualitySettings.SetQualityLevel(quality);

    }

    //set fullscreen
    public void setfullscreen(bool isfull)
    {
        Screen.fullScreen = isfull;
    }
    
    //set resolution
    public void setres(int resindex)
    {
        Resolution resolution = resolutions[resindex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    //set sensivity
    public void SetSensivity(float sensivity)
    {
        cs.sensivity = sensivity;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{   
    //used to play the music in scene2
    //author ~~Jonathan

    //the sounds
    public AudioSource bgm;
    public AudioSource mouseclick;

    public bool soundplayed;

    private void Start()
    {
        //bgm.Play();
    }

    //plays the mouseclick; gets triggered on button click
    public void MouseClick()
    {
        /*soundplayed = true;

        if(soundplayed == true)
        {
            mouseclick.Play();
            soundplayed = false;
        }
        */

        mouseclick.Play();
    }
}
